ConfiguredDaemonSet
===================

Requirements
------------

API Requirements
^^^^^^^^^^^^^^^^

- Pod template
- Set of nodes on which Pods are to be spawned
- ConfigMap/Secret volume mounts which can be configured per-node

Operative Requirements
^^^^^^^^^^^^^^^^^^^^^^

- Per configured node, there MUST NOT exist more than one running Pod of a
  ConfiguredDaemonSet for a longer period of time. The controller MUST attempt
  to dispose of excess pods immediately.
- Pods of a ConfiguredDaemonSet MUST NOT run on any node not configured
  explicitly in the ConfiguredDaemonSet spec.
- Upon configuration changes, the controller MUST execute a rolling upgrade of
  the configuration. The maximum number of Pods which are in the process of
  being restarted due to the upgrade at the same time is configurable and MUST
  NOT be exceeded.
- The controller MUST be able to deal with configuration changes which happen
  while a rolling upgrade for an earlier configuration change is still in
  progress.
- Invalid configuration (e.g. inconsistent node sets in different config blocks)
  MUST NOT be applied.
- The controller MAY start pods even if the Secret / ConfigMap they use is not
  yet present. The downtime caused by that is covered by the maximum number of
  pods which can be redeployed at the same time.
