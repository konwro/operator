Why is the Yaook Nova Container privileged?
===========================================

The following is the result of an investigation whether it would be possible to drop the privileged setting (https://gitlab.com/yaook/operator/-/issues/398)

TL;DR: It is not possible due to the required Bidirectional mounts.

Current situation
-----------------

The following container in the nova-compute pod of the Nova deployment require to be run as privileged: nova-compute, nova-libvirtd

The privileged is needed by the following requirements:

- Requirement 1: Nova must have access to /dev/kvm

- Requirement 2: Nova and libvirt communicate via sockets

- Requirement 3: Nova Container uses Bidirectional mount propagation (see [2], [3])


The following mounts can be found in the nova-compute-Container and libvirtd-Container spec:

            - name: varlibnova

              mountPath: /var/lib/nova

              mountPropagation: Bidirectional

            - name: varliblibvirt

              mountPath: /var/lib/libvirt

              mountPropagation: Bidirectional


Nova uses instance-related mounts in /var/lib/nova/instances e.g. to share filesystems on different hosts. 

This enables the k8s-host to unmount mounted filesystems when the pod/container using the mount does no longer exist (e.g. crashed).

libvirtd and nova-compute must operate on the same filesystems, even those that are mounted at a later time within a container. (This can only be implemented by using Bidirectional mounts.)

A good explanation on  bidirectional mounts can be found in [4].

Possible workarounds
--------------------

Requirement 1:

/dev/kvm could be provided to a non-privileged host using a Kubernetes device plugin [1].

Requirement 2:

Possible workarounds were not researched as Requirement 3 already provides the reason for the containers to be privileged.

Requirement 3:

There is no good workaround other than not to use Bidirectional mounts, which is not an option here. Bidirectional mounts always require privileged containers [5].

Risk assessment
---------------

If you would use nova directly on the host, the mount points would be accessible as well. Malicious access to the node through nova would pose the same risc (full system access) as malicious access to nova via the privileged pod.

Conclusion
----------

Running these containers as privileged is necessary as long as bidirectional mounts are only supported by Kubernetes in privileged containers.

[1] [https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/)

[2] [https://gitlab.com/yaook/operator/-/blob/a61e26c5082671e3cdab18e42f6a425b647a8c22/yaook/op/nova_compute/templates/compute-statefulset.yaml#L253](https://gitlab.com/yaook/operator/-/blob/a61e26c5082671e3cdab18e42f6a425b647a8c22/yaook/op/nova_compute/templates/compute-statefulset.yaml#L253)

[3] [https://gitlab.com/yaook/operator/-/blob/a61e26c5082671e3cdab18e42f6a425b647a8c22/yaook/op/nova_compute/templates/compute-statefulset.yaml#L140](https://gitlab.com/yaook/operator/-/blob/a61e26c5082671e3cdab18e42f6a425b647a8c22/yaook/op/nova_compute/templates/compute-statefulset.yaml#L140)

[4] [https://medium.com/kokster/kubernetes-mount-propagation-5306c36a4a2d](https://medium.com/kokster/kubernetes-mount-propagation-5306c36a4a2d)

[5] [https://kubernetes.io/docs/concepts/storage/volumes/#mount-propagation](https://kubernetes.io/docs/concepts/storage/volumes/#mount-propagation)
