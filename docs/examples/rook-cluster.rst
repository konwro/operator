Ceph Cluster
============

.. literalinclude:: rook-cluster.yaml

Ceph Resources
==============

.. literalinclude:: rook-resources.yaml
