#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import json
import subprocess
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import yaook.statemachine.resources.helm as helm


class Testtemporary_json_file(unittest.TestCase):
    def test_makes_data_available_at_given_path(self):
        data = {
            "foo": {
                "bar": ["baz", 1, 2, 3],
            }
        }
        with helm.temporary_json_file(data) as f:
            self.assertIsNotNone(f)
            with open(f, "r") as fin:
                data_in = json.load(fin)

            self.assertEqual(data_in, data)

    def test_uses_named_temporary_file(self):
        with contextlib.ExitStack() as stack:
            tempfile = unittest.mock.Mock(["write", "flush", "name"])

            ntf = stack.enter_context(unittest.mock.patch(
                "tempfile.NamedTemporaryFile",
                new=unittest.mock.MagicMock(),
            ))
            ntf.return_value.__enter__.return_value = tempfile

            with helm.temporary_json_file({}) as f:
                # sequencing of the calls is tested by the other test which
                # requires that the data is readable
                tempfile.write.assert_called_once_with("{}")
                tempfile.flush.assert_called_once_with()
                self.assertEqual(f, tempfile.name)


@ddt.ddt
class Testescape_helm_string(unittest.TestCase):
    @ddt.data(
        ("foo", '"foo"'),
        ("foo bar", '"foo bar"'),
        ("foo: \"bar\"", '"foo: \\"bar\\""'),
        ("foo\nbar", '"foo\\nbar"'),
        ("foo\x01bar", '"foo\\x01bar"'),
        ("foo\u2010bar", '"foo\u2010bar"'),
    )
    @ddt.unpack
    def test_escapes_string_correctly(self, raw, escaped):
        self.assertEqual(
            helm.escape_helm_string(raw),
            escaped,
            raw,
        )


class Testas_helm_template(unittest.TestCase):
    @unittest.mock.patch("yaook.statemachine.helm.escape_helm_string")
    def test_wraps_escaped_string_in_print(self, escape_helm_string):
        escape_helm_string.return_value = "escaped string"

        self.assertEqual(
            helm.as_helm_template(sentinel.input_string),
            "{{ print escaped string }}",
        )

        escape_helm_string.assert_called_once_with(sentinel.input_string)


class Testmanaged_subprocess(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.create_subprocess = unittest.mock.AsyncMock([])
        self.process = unittest.mock.Mock(asyncio.subprocess.Process)
        self.process.returncode = 0
        self.create_subprocess.return_value = self.process
        self.patches = [
            unittest.mock.patch("asyncio.create_subprocess_exec",
                                new=self.create_subprocess),
        ]
        self.active_patches = []
        self._patch()

    def tearDown(self):
        for p in self.active_patches:
            p.stop()

    def _patch(self):
        for p in self.patches:
            p.start()
            self.active_patches.append(p)

    async def test_creates_subprocess_and_yields_it(self):
        async with helm.managed_subprocess(
                unittest.mock.sentinel.prog,
                unittest.mock.sentinel.arg0,
                unittest.mock.sentinel.arg1,
                foo=unittest.mock.sentinel.bar) as proc:
            self.assertEqual(proc, self.process)
            self.create_subprocess.assert_awaited_once_with(
                unittest.mock.sentinel.prog,
                unittest.mock.sentinel.arg0,
                unittest.mock.sentinel.arg1,
                foo=unittest.mock.sentinel.bar
            )

    async def test_kills_subprocess_if_not_completed_at_exit(self):
        self.process.returncode = None

        async with helm.managed_subprocess(
                unittest.mock.sentinel.prog,
                unittest.mock.sentinel.arg0,
                unittest.mock.sentinel.arg1,
                foo=unittest.mock.sentinel.bar):
            pass

        self.process.kill.assert_called_once_with()
        self.process.wait.assert_awaited_once_with()

    async def test_does_not_kill_subprocess_if_completed_at_exit(self):
        self.process.returncode = None

        async with helm.managed_subprocess(
                unittest.mock.sentinel.prog,
                unittest.mock.sentinel.arg0,
                unittest.mock.sentinel.arg1,
                foo=unittest.mock.sentinel.bar):
            self.process.returncode = 1

        self.process.kill.assert_not_called()
        self.process.wait.assert_not_awaited()


class Testcheck_communicate(unittest.IsolatedAsyncioTestCase):
    async def test_calls_communicate_and_returns_result_on_success(self):
        proc = unittest.mock.Mock(asyncio.subprocess.Process)
        proc.communicate.return_value = (
            unittest.mock.sentinel.stdout,
            unittest.mock.sentinel.stderr,
        )
        proc.returncode = 0
        argv = unittest.mock.sentinel.argv

        result = await helm.check_communicate(proc, argv)

        proc.communicate.assert_awaited_once_with()

        self.assertEqual(
            result,
            (unittest.mock.sentinel.stdout, unittest.mock.sentinel.stderr),
        )

    async def test_calls_communicate_and_raises_CalledProcessError_on_failure(
            self):
        proc = unittest.mock.Mock(asyncio.subprocess.Process)
        proc.communicate.return_value = (
            unittest.mock.sentinel.stdout,
            unittest.mock.sentinel.stderr,
        )
        proc.returncode = unittest.mock.sentinel.nonzero
        argv = unittest.mock.sentinel.argv

        with self.assertRaises(subprocess.CalledProcessError) as exc_info:
            await helm.check_communicate(proc, argv)

        proc.communicate.assert_awaited_once_with()

        self.assertEqual(exc_info.exception.cmd, unittest.mock.sentinel.argv)
        self.assertEqual(exc_info.exception.returncode,
                         unittest.mock.sentinel.nonzero)
        self.assertEqual(exc_info.exception.output,
                         unittest.mock.sentinel.stdout)
        self.assertEqual(exc_info.exception.stderr,
                         unittest.mock.sentinel.stderr)


class Testhelm_upgrade(unittest.IsolatedAsyncioTestCase):
    async def test_invokes_helm_upgrade_with_install(self):
        info = helm.HelmReleaseInfo(
            name="resource name",
            namespace="some namespace",
            chart="magic/chart",
            version="that version",
            values=unittest.mock.sentinel.values,
        )
        process = unittest.mock.Mock(asyncio.subprocess.Process)

        with contextlib.ExitStack() as stack:
            managed_subprocess = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.managed_subprocess",
                new=unittest.mock.MagicMock(
                    ["__aenter__", "__aexit__"]
                ),
            ))
            managed_subprocess.return_value.__aenter__.return_value = process

            temporary_json_file = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.temporary_json_file",
                new=unittest.mock.MagicMock(
                    ["__enter__", "__exit__"],
                ),
            ))
            temporary_json_file.return_value.__enter__.return_value = \
                "magic-json-file.json"

            check_communicate = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.check_communicate",
            ))

            await helm.helm_upgrade(info)

        temporary_json_file.assert_called_once_with(info.values)
        managed_subprocess.assert_called_once_with(
            "helm", "upgrade", "--install",
            "--values", "magic-json-file.json",
            "--namespace", "some namespace",
            "--version", "that version",
            "resource name",
            "magic/chart",
        )
        check_communicate.assert_awaited_once_with(
            process,
            unittest.mock.ANY,
        )

        self.assertSequenceEqual(
            # positional argument list (1) of first call (0)
            managed_subprocess.mock_calls[0][1],
            # second (1) positional argument (0) of first call (0)
            # note that await args list do not have the call name, which is
            # why the list of positional arguments is at index 0 and not at
            # index 1
            check_communicate.await_args_list[0][0][1],
        )

    async def test_invokes_helm_upgrade_with_install_and_omits_version_if_unsed(self):  # noqa
        info = helm.HelmReleaseInfo(
            name="resource name",
            namespace="some namespace",
            chart="magic/chart",
            version=None,
            values=unittest.mock.sentinel.values,
        )
        process = unittest.mock.Mock(asyncio.subprocess.Process)

        with contextlib.ExitStack() as stack:
            managed_subprocess = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.managed_subprocess",
                new=unittest.mock.MagicMock(
                    ["__aenter__", "__aexit__"]
                ),
            ))
            managed_subprocess.return_value.__aenter__.return_value = process

            temporary_json_file = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.temporary_json_file",
                new=unittest.mock.MagicMock(
                    ["__enter__", "__exit__"],
                ),
            ))
            temporary_json_file.return_value.__enter__.return_value = \
                "magic-json-file.json"

            check_communicate = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.check_communicate",
            ))

            await helm.helm_upgrade(info)

        temporary_json_file.assert_called_once_with(info.values)
        managed_subprocess.assert_called_once_with(
            "helm", "upgrade", "--install",
            "--values", "magic-json-file.json",
            "--namespace", "some namespace",
            "resource name",
            "magic/chart",
        )
        check_communicate.assert_awaited_once_with(
            process,
            unittest.mock.ANY,
        )

        self.assertSequenceEqual(
            # positional argument list (1) of first call (0)
            managed_subprocess.mock_calls[0][1],
            # second (1) positional argument (0) of first call (0)
            # note that await args list do not have the call name, which is
            # why the list of positional arguments is at index 0 and not at
            # index 1
            check_communicate.await_args_list[0][0][1],
        )


class Testhelm_delete(unittest.IsolatedAsyncioTestCase):
    async def test_invokes_helm_delete(self):
        process = unittest.mock.Mock(asyncio.subprocess.Process)

        with contextlib.ExitStack() as stack:
            managed_subprocess = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.managed_subprocess",
                new=unittest.mock.MagicMock(
                    ["__aenter__", "__aexit__"]
                ),
            ))
            managed_subprocess.return_value.__aenter__.return_value = process

            check_communicate = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.check_communicate",
            ))

            await helm.helm_delete(
                "some namespace",
                "resource name",
            )

        managed_subprocess.assert_called_once_with(
            "helm", "delete",
            "--namespace", "some namespace",
            "resource name",
        )
        check_communicate.assert_awaited_once_with(
            process,
            unittest.mock.ANY,
        )

        self.assertSequenceEqual(
            # positional argument list (1) of first call (0)
            managed_subprocess.mock_calls[0][1],
            # second (1) positional argument (0) of first call (0)
            # note that await args list do not have the call name, which is
            # why the list of positional arguments is at index 0 and not at
            # index 1
            check_communicate.await_args_list[0][0][1],
        )


class TestHelmRelease(unittest.IsolatedAsyncioTestCase):
    class HelmReleaseTest(helm.HelmRelease):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.get_helm_release_mock = unittest.mock.AsyncMock([])

        async def _get_helm_release(self, ctx, dependencies):
            return await self.get_helm_release_mock(ctx, dependencies)

    def setUp(self):
        self.hr = self.HelmReleaseTest(
            finalizer="foo.baz/bar"
        )

    async def test_update_calls_helm_upgrade_with_info(self):
        self.hr.get_helm_release_mock.return_value = \
            unittest.mock.sentinel.info

        with contextlib.ExitStack() as stack:
            helm_upgrade = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.helm_upgrade",
            ))

            await self.hr.update(unittest.mock.sentinel.ctx,
                                 unittest.mock.sentinel.deps)

        self.hr.get_helm_release_mock.assert_awaited_once_with(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.deps
        )

        helm_upgrade.assert_awaited_once_with(unittest.mock.sentinel.info)

    async def test_delete_calls_helm_delete_with_namespace_and_name(self):
        info = unittest.mock.Mock(["namespace", "name"])
        self.hr.get_helm_release_mock.return_value = info

        with contextlib.ExitStack() as stack:
            helm_delete = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.helm.helm_delete",
            ))

            await self.hr.delete(unittest.mock.sentinel.ctx,
                                 unittest.mock.sentinel.deps)

        self.hr.get_helm_release_mock.assert_awaited_once_with(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.deps
        )

        helm_delete.assert_awaited_once_with(
            info.namespace,
            info.name,
        )
