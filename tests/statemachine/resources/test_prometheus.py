import unittest
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.prometheus as prometheus


class TestGeneratedServiceMonitor(unittest.IsolatedAsyncioTestCase):
    def test___init___declares_dependencies(self):
        gsms = prometheus.GeneratedServiceMonitor(
            "name", sentinel.service,
            sentinel.certificate, ["endpoint"])

        self.assertIn(sentinel.service, gsms._dependencies)
        self.assertIn(sentinel.certificate, gsms._dependencies)

    async def test__make_body_with_service_labels(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")
        sentinel.ctx.parent_spec = unittest.mock.Mock()
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "serviceMonitor": {
                "additionalLabels": {
                    "foo": "bar"
                    }
                }
            }

        service.labels = unittest.mock.Mock()
        service.labels.return_value = ["labela", "labelb"]
        certificate = unittest.mock.Mock()
        certificate.get = unittest.mock.AsyncMock()
        certificate.get.return_value = kclient.V1ObjectReference(
            name="certificatename")

        gsms = prometheus.GeneratedServiceMonitor(
            "name",
            service,
            certificate,
            ["endpointname"])

        self.assertEqual(
            {
                "apiVersion": "monitoring.coreos.com/v1",
                "kind": "ServiceMonitor",
                "metadata": {
                    "labels": {
                        "foo": "bar"
                    },
                    "name": "name",
                },
                "spec": {
                    "selector": {
                        "matchLabels": [
                            "labela",
                            "labelb"
                        ]
                    },
                    "endpoints": [
                        {
                            "port": "endpointname",
                            "scheme": "https",
                            "tlsConfig": {
                                "ca": {
                                    "secret": {
                                        "name": "certificatename",
                                        "key": "tls.crt",
                                    },
                                },
                                "serverName": "servicename",
                            },
                        }
                    ],
                },
            },
            await gsms._make_body(ctx, sentinel.deps)
        )

    async def test__make_body_with_server_name_provider(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")
        sentinel.ctx.parent_spec = unittest.mock.Mock()
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "api": {
                "ingress": {
                    "fqdn": "my.test.url"
                }
            }
        }

        service.labels = unittest.mock.Mock()
        service.labels.return_value = ["labela", "labelb"]
        certificate = unittest.mock.Mock()
        certificate.get = unittest.mock.AsyncMock()
        certificate.get.return_value = kclient.V1ObjectReference(
            name="certificatename")

        gsms = prometheus.GeneratedServiceMonitor(
            metadata="name",
            service=service,
            certificate=certificate,
            endpoints=["endpointname"],
            server_name_provider=lambda ctx: (
                ctx.parent_spec["api"]["ingress"]["fqdn"]),
            )

        self.assertEqual(
            {
                "apiVersion": "monitoring.coreos.com/v1",
                "kind": "ServiceMonitor",
                "metadata": {
                    "labels": {},
                    "name": "name",
                },
                "spec": {
                    "selector": {
                        "matchLabels": [
                            "labela",
                            "labelb"
                        ]
                    },
                    "endpoints": [
                        {
                            "port": "endpointname",
                            "scheme": "https",
                            "tlsConfig": {
                                "ca": {
                                    "secret": {
                                        "name": "certificatename",
                                        "key": "tls.crt",
                                    },
                                },
                                "serverName": "my.test.url",
                            },
                        }
                    ],
                },
            },
            await gsms._make_body(ctx, sentinel.deps)
        )

    async def test__needs_update_returns_true_on_label_change(self):
        gsms = prometheus.GeneratedServiceMonitor(
            "name", sentinel.service,
            sentinel.certificate, ["endpoint"])
        self.assertTrue(gsms._needs_update(
            {
                "metadata": {
                    "labels": {
                        "a": "b",
                    },
                },
                "spec": {},
            },
            {
                "metadata": {
                    "labels": {
                        "a": "c",
                    },
                },
                "spec": {},
            },
        ))


class TestGeneratedStatefulsetServiceMonitor(unittest.IsolatedAsyncioTestCase):
    def test___init___declares_dependencies(self):
        gsms = prometheus.GeneratedStatefulsetServiceMonitor(
            "name", sentinel.service,
            sentinel.certificate, ["endpoint"])

        self.assertIn(sentinel.service, gsms._dependencies)

    async def test__make_body_relabel_instance_label(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")
        sentinel.ctx.parent_spec = unittest.mock.Mock()
        ctx = unittest.mock.Mock(["parent_spec"])
        ctx.parent_spec = {
            "serviceMonitor": {
                "additionalLabels": {
                    "foo": "bar"
                    }
                }
            }

        service.labels = unittest.mock.Mock()
        service.labels.return_value = ["labela", "labelb"]
        certificate = unittest.mock.Mock()
        certificate.get = unittest.mock.AsyncMock()
        certificate.get.return_value = kclient.V1ObjectReference(
            name="certificatename")

        gssm = prometheus.GeneratedStatefulsetServiceMonitor(
            "name",
            service,
            certificate,
            ["endpointname"])

        self.assertEqual(
            {
                "apiVersion": "monitoring.coreos.com/v1",
                "kind": "ServiceMonitor",
                "metadata": {
                    "labels": {
                        "foo": "bar"
                    },
                    "name": "name",
                },
                "spec": {
                    "selector": {
                        "matchLabels": [
                            "labela",
                            "labelb"
                        ]
                    },
                    "endpoints": [
                        {
                            "port": "endpointname",
                            "scheme": "https",
                            "tlsConfig": {
                                "ca": {
                                    "secret": {
                                        "name": "certificatename",
                                        "key": "tls.crt",
                                    },
                                },
                                "serverName": "servicename",
                            },
                            "relabelings": [
                                {
                                    "action": "replace",
                                    "separator": ':',
                                    "sourceLabels": [
                                        "__meta_kubernetes_pod_name",
                                        "__meta_kubernetes_pod_container"
                                        "_port_number",
                                    ],
                                    "targetLabel": "instance",
                                },
                            ],
                        }
                    ],
                },
            },
            await gssm._make_body(ctx, sentinel.deps)
        )
