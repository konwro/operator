import contextlib
import unittest
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.watcher as watcher

import yaook.statemachine.resources.dependencies as dependencies
import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.yaook as yaook


class TestForeignReference(unittest.IsolatedAsyncioTestCase):
    class ForeignReferenceTest(dependencies.ForeignReference):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.get_resource_references_mock = unittest.mock.Mock([])

        def get_resource_references(self, ctx):
            return self.get_resource_references_mock(ctx)

    def setUp(self):
        self.rif = unittest.mock.Mock([])
        self.ri = unittest.mock.Mock(interfaces.ResourceInterface)
        self.rif.return_value = self.ri
        self.fr = self.ForeignReferenceTest(
            resource_interface_factory=self.rif,
            component=sentinel.component,
        )

    def test__create_resource_interface_calls_factory(self):
        result = self.fr._create_resource_interface(sentinel.api_client)

        self.rif.assert_called_once_with(sentinel.api_client)

        self.assertEqual(result, self.rif())

    async def test_reconile_returns_false(self):
        self.assertFalse(await self.fr.reconcile(sentinel.ctx, sentinel.deps))

    async def test_get_raises_ResourceNotPresent_if_no_reference_returned(self):  # noqa
        ctx = unittest.mock.Mock()
        self.fr.get_resource_references_mock.return_value = []

        with self.assertRaises(exceptions.ResourceNotPresent):
            await self.fr.get(ctx)

        self.fr.get_resource_references_mock.assert_called_once_with(ctx)

    async def test_get_raises_AmbiguousRequest_if_references_returned(self):
        ctx = unittest.mock.Mock()
        self.fr.get_resource_references_mock.return_value = [
            sentinel.object1,
            sentinel.object2,
        ]

        with self.assertRaises(exceptions.AmbiguousRequest):
            await self.fr.get(ctx)

        self.fr.get_resource_references_mock.assert_called_once_with(ctx)

    async def test_get_returns_single_reference(self):
        ctx = unittest.mock.Mock()
        self.fr.get_resource_references_mock.return_value = [
            sentinel.object1,
        ]

        result = await self.fr.get(ctx)

        self.fr.get_resource_references_mock.assert_called_once_with(ctx)
        self.assertEqual(result, sentinel.object1)

    async def test_get_all_is_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            await self.fr.get_all(sentinel.ctx)

    async def test_delete_is_most_likely_noop(self):
        await self.fr.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.fr.cleanup_orphans(sentinel.ctx, sentinel.protect)


@ddt.ddt
class TestReadyForeignReference(unittest.IsolatedAsyncioTestCase):
    class ForeignReferenceTest(dependencies.ReadyForeignReference):
        def __init__(self, **kwargs):
            super().__init__(**kwargs)
            self.get_resource_references_mock = unittest.mock.Mock([])

        def get_resource_references(self, ctx):
            return self.get_resource_references_mock(ctx)

    def setUp(self):
        self.fr = self.ForeignReferenceTest(
            resource_interface_factory=None,
            component=sentinel.component,
        )

    def test_uses_yaook_resource_readyness(self):
        self.assertIsInstance(self.fr, yaook.YaookResourceReadinessMixin)


class TestForeignResourceDependency(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.fr = unittest.mock.Mock(k8s.KubernetesReference)
        self.fri = unittest.mock.Mock(interfaces.ResourceInterface)
        self.fri.api_version = "foo.example/v1random1"
        self.fri.plural = sentinel.foreign_plural
        self.fri.group = "foo.example"
        self.fri.version = "v1random1"
        self.fr.get_resource_interface.return_value = self.fri
        self.rif = unittest.mock.Mock([])
        self.ri = unittest.mock.Mock(interfaces.ResourceInterface)
        self.rif.return_value = self.ri
        self.frd = dependencies.ForeignResourceDependency(
            component=sentinel.local_comp,
            foreign_resource=self.fr,
            foreign_component=sentinel.foreign_comp,
            resource_interface_factory=self.rif,
        )

    def test_is_kubernetes_reference(self):
        self.assertIsInstance(self.frd, k8s.KubernetesReference)

    def test__create_resource_interface_calls_factory(self):
        result = self.frd._create_resource_interface(sentinel.api_client)

        self.rif.assert_called_once_with(sentinel.api_client)
        self.assertEqual(result, self.rif())

    def test__static_labels_returns_labels_which_do_not_need_the_name(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])

        result = self.frd._static_labels(ctx)

        self.fr.get_resource_interface.assert_called_once_with(ctx)
        self.fr.get.assert_not_called()

        self.assertDictEqual(
            result,
            {
                context.LABEL_COMPONENT: sentinel.foreign_comp,
                context.LABEL_PARENT_PLURAL: sentinel.foreign_plural,
                context.LABEL_PARENT_GROUP: "foo.example",
            },
        )

    async def test__labels_composes_labels_from_the_foreign_resource(self):
        self.fr.get.return_value = kclient.V1ObjectReference(
            name=sentinel.foreign_name,
            namespace=sentinel.foreign_namespace,
        )

        ctx = unittest.mock.Mock(["api_client", "namespace"])

        result = await self.frd._labels(ctx)

        self.fr.get_resource_interface.assert_called_once_with(ctx)
        self.fr.get.assert_awaited_once_with(ctx)

        self.assertDictEqual(
            result,
            {
                context.LABEL_COMPONENT: sentinel.foreign_comp,
                context.LABEL_PARENT_NAME: sentinel.foreign_name,
                context.LABEL_PARENT_PLURAL: sentinel.foreign_plural,
                context.LABEL_PARENT_GROUP: "foo.example",
            },
        )

    async def test__labels_uses_static_labels(self):
        self.fr.get.return_value = kclient.V1ObjectReference(
            name=sentinel.foreign_name,
            namespace=sentinel.foreign_namespace,
        )

        ctx = unittest.mock.Mock(["api_client", "namespace"])

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                sentinel.l1: sentinel.v1,
                sentinel.l2: sentinel.v2,
            }

            result = await self.frd._labels(ctx)

        _static_labels.assert_called_once_with(ctx)
        self.fr.get.assert_awaited_once_with(ctx)

        self.assertDictEqual(
            result,
            {
                context.LABEL_PARENT_NAME: sentinel.foreign_name,
                sentinel.l1: sentinel.v1,
                sentinel.l2: sentinel.v2,
            },
        )

    async def test__get_all_returns_all_instances_selected_with_labels(self):
        ctx = unittest.mock.Mock()
        self.ri.list_.return_value = sentinel.listing

        with contextlib.ExitStack() as stack:
            _labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_labels",
            ))
            _labels.return_value = {
                "foo": "bar",
                "baz": "fnord",
            }

            result = await self.frd._get_all(ctx)

        self.rif.assert_called_once_with(ctx.api_client)
        self.ri.list_.assert_awaited_once_with(
            ctx.namespace,
            label_selector="foo=bar,baz=fnord",
        )
        _labels.assert_awaited_once_with(ctx)

        self.assertEqual(result, sentinel.listing)

    async def test__get_all_wraps_ResourceNotFound_from_labels_in_DependencyNotReady(self):  # noqa
        ctx = unittest.mock.Mock()
        self.ri.list_.return_value = sentinel.listing

        exc = exceptions.ResourceNotPresent(
            sentinel.component,
            unittest.mock.Mock(),
        )

        with contextlib.ExitStack() as stack:
            _labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_labels",
            ))
            _labels.side_effect = exc

            with self.assertRaises(dependencies.DependencyNotReady):
                await self.frd._get_all(ctx)

        self.ri.list_.assert_not_called()

    async def test_get_uses__get_all_and_returns_exactly_one_match(self):
        ctx = sentinel.ctx

        with contextlib.ExitStack() as stack:
            _get_all = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_get_all",
            ))
            _get_all.return_value = [
                sentinel.object1,
            ]

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "namespace": sentinel.namespace,
                "name": sentinel.name,
            }

            result = await self.frd.get(ctx)

        _get_all.assert_awaited_once_with(ctx)
        extract_metadata.assert_called_once_with(sentinel.object1)

        self.assertEqual(result.name, sentinel.name)
        self.assertEqual(result.namespace, sentinel.namespace)

    async def test_get_raises_DependencyNotReady_if_no_objects_returned(self):
        # those are all needed to raise a proper DependencyNotReady
        ctx = unittest.mock.Mock(["parent_kind", "parent_api_version",
                                  "namespace", "parent_name",
                                  "instance"])

        with contextlib.ExitStack() as stack:
            _get_all = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_get_all",
            ))
            _get_all.return_value = []

            with self.assertRaises(dependencies.DependencyNotReady):
                await self.frd.get(ctx)

        _get_all.assert_awaited_once_with(ctx)

    async def test_get_raises_AmbiguousRequest_if_multiple_matches(self):
        # those are all needed to raise a proper AmbiguousRequest
        ctx = unittest.mock.Mock(["parent_kind", "parent_api_version",
                                  "namespace", "parent_name",
                                  "instance"])

        with contextlib.ExitStack() as stack:
            _get_all = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_get_all",
            ))
            _get_all.return_value = [
                sentinel.object1,
                sentinel.object2,
                sentinel.object3,
            ]

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))

            with self.assertRaises(exceptions.AmbiguousRequest):
                await self.frd.get(ctx)

        _get_all.assert_awaited_once_with(ctx)
        extract_metadata.assert_not_called()

    async def test_get_all_uses__get_all_and_metadata_of_binned_entries(self):
        ctx = sentinel.ctx

        def extract_metadata_impl(obj):
            return {
                "name": obj,
                "namespace": sentinel.namespace,
                "labels": {
                    context.LABEL_INSTANCE: f"i-{obj}",
                },
            }

        with contextlib.ExitStack() as stack:
            _get_all = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_get_all",
            ))
            _get_all.return_value = [
                sentinel.object1,
                sentinel.object2,
                sentinel.object3,
            ]

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_impl

            result = await self.frd.get_all(ctx)

        _get_all.assert_awaited_once_with(ctx)
        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(sentinel.object1),
                unittest.mock.call(sentinel.object2),
                unittest.mock.call(sentinel.object3),
            ],
        )

        self.assertDictEqual(
            result,
            {
                "i-sentinel.object1": kclient.V1ObjectReference(
                    name=sentinel.object1,
                    namespace=sentinel.namespace,
                ),
                "i-sentinel.object2": kclient.V1ObjectReference(
                    name=sentinel.object2,
                    namespace=sentinel.namespace,
                ),
                "i-sentinel.object3": kclient.V1ObjectReference(
                    name=sentinel.object3,
                    namespace=sentinel.namespace,
                ),
            },
        )

    async def test_get_all_handles_absent_label_correctly(self):
        ctx = sentinel.ctx

        def extract_metadata_impl(obj):
            return {
                "name": obj,
                "namespace": sentinel.namespace,
            }

        with contextlib.ExitStack() as stack:
            _get_all = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_get_all",
            ))
            _get_all.return_value = [
                sentinel.object1,
            ]

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_impl

            result = await self.frd.get_all(ctx)

        _get_all.assert_awaited_once_with(ctx)
        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(sentinel.object1),
            ],
        )

        self.assertDictEqual(
            result,
            {
                None: kclient.V1ObjectReference(
                    name=sentinel.object1,
                    namespace=sentinel.namespace,
                ),
            },
        )

    async def test_reconcile_returns_false(self):
        self.assertFalse(await self.frd.reconcile(sentinel.ctx, sentinel.deps))

    async def test_is_ready_returns_true_if_get_passes(self):
        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.frd, "get",
            ))
            get.return_value = sentinel.ref

            result = await self.frd.is_ready(sentinel.ctx)

        get.assert_awaited_once_with(sentinel.ctx)
        self.assertTrue(result)

    async def test_is_ready_returns_true_if_get_raises_AmbiguousRequest(self):
        ctx = unittest.mock.Mock()
        exc = exceptions.AmbiguousRequest(sentinel.component, ctx)

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.frd, "get",
            ))
            get.side_effect = exc

            result = await self.frd.is_ready(ctx)

        self.assertTrue(result)

    async def test_is_ready_returns_false_if_get_raises_DependencyNotReady(self):  # noqa
        ctx = unittest.mock.Mock()
        exc = dependencies.DependencyNotReady(sentinel.component, ctx)

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.frd, "get",
            ))
            get.side_effect = exc

            result = await self.frd.is_ready(ctx)

        self.assertFalse(result)

    async def test_is_ready_lets_ResourceNotPresent_slip_through(self):
        ctx = unittest.mock.Mock()
        exc = exceptions.ResourceNotPresent(sentinel.component, ctx)

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.frd, "get",
            ))
            get.side_effect = exc

            with self.assertRaises(exceptions.ResourceNotPresent):
                await self.frd.is_ready(ctx)

    def test_registers_listener_for_resource_type(self):
        self.ri.api_version = "foo.bar/v1random1"
        self.ri.plural = sentinel.objects
        self.ri.group = "foo.bar"
        self.ri.version = "v1random1"

        with contextlib.ExitStack() as stack:
            super_listeners = stack.enter_context(unittest.mock.patch.object(
                k8s.KubernetesReference, "get_listeners",
            ))
            super_listeners.return_value = [sentinel.foo]

            result = self.frd.get_listeners()

        self.assertIn(sentinel.foo, result)
        self.assertIn(
            context.KubernetesListener(
                api_group="foo.bar",
                version="v1random1",
                plural=sentinel.objects,
                listener=self.frd._handle_foreign_component_event,
                broadcast=False,
                broadcast_filter={
                    'state.yaook.cloud/parent-group': self.frd
                        ._foreign_resource._autocreate_resource_interface()
                        .group,
                    'state.yaook.cloud/parent-plural': self.frd
                        ._foreign_resource._autocreate_resource_interface()
                        .plural,
                    'state.yaook.cloud/component': sentinel.foreign_comp
                    }
            ),
            result,
        )

    def test__handle_foreign_component_event_returns_false_for_unrelated_added_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.ADDED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "notvalue2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        _static_labels.assert_called_once_with(sentinel.ctx)
        extract_metadata.assert_called_once_with(ev.object_)

        self.assertFalse(result)

    def test__handle_foreign_component_event_returns_false_for_unrelated_modified_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.MODIFIED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "notvalue2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        _static_labels.assert_called_once_with(sentinel.ctx)
        extract_metadata.assert_called_once_with(ev.object_)

        self.assertFalse(result)

    def test__handle_foreign_component_event_returns_false_for_unrelated_deleted_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.DELETED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "notvalue2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        self.assertFalse(result)

    def test__handle_foreign_component_event_returns_true_for_matching_added_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.ADDED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "value2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        _static_labels.assert_called_once_with(sentinel.ctx)
        extract_metadata.assert_called_once_with(ev.object_)

        self.assertTrue(result)

    def test__handle_foreign_component_event_returns_true_for_matching_modified_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.MODIFIED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "value2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        _static_labels.assert_called_once_with(sentinel.ctx)
        extract_metadata.assert_called_once_with(ev.object_)

        self.assertTrue(result)

    def test__handle_foreign_component_event_handles_missing_labels_without_crash(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.MODIFIED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {}

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        _static_labels.assert_called_once_with(sentinel.ctx)
        extract_metadata.assert_called_once_with(ev.object_)

        self.assertFalse(result)

    def test__handle_foreign_component_event_returns_false_for_matching_deleted_object(self):  # noqa
        ev = unittest.mock.Mock(["type_", "object_"])
        ev.type_ = watcher.EventType.DELETED

        with contextlib.ExitStack() as stack:
            _static_labels = stack.enter_context(unittest.mock.patch.object(
                self.frd, "_static_labels",
            ))
            _static_labels.return_value = {
                "label1": "value1",
                "label2": "value2",
            }

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "labels": {
                    "label1": "value1",
                    "label2": "value2",
                    "otherlabel": "othervalue",
                }
            }

            result = self.frd._handle_foreign_component_event(
                sentinel.ctx,
                ev,
            )

        self.assertFalse(result)

    async def test_delete_is_most_likely_noop(self):
        await self.frd.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.frd.cleanup_orphans(sentinel.ctx, sentinel.protect)


class TestKeystoneReference(unittest.TestCase):
    def setUp(self):
        self.kr = dependencies.KeystoneReference()

    def test_is_ready_foreign_reference(self):
        self.assertIsInstance(self.kr, dependencies.ReadyForeignReference)

    def test__create_resource_interface_raises(self):
        self.assertRaises(
            NotImplementedError,
            self.kr._create_resource_interface,
            sentinel.api_client
        )

    def test_references_keystone(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "keystoneRef": {"kind": "KeystoneDeployment"},
        }
        interface = self.kr.get_resource_interface(ctx)
        self.assertEquals(
            interface.api_version,
            interfaces.keystonedeployment_interface(ctx.api_client).api_version
        )
        self.assertEquals(
            interface.plural,
            interfaces.keystonedeployment_interface(ctx.api_client).plural
        )

    def test_references_external_keystone(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "keystoneRef": {"kind": "ExternalKeystoneDeployment"},
        }
        interface = self.kr.get_resource_interface(ctx)
        self.assertEquals(
            interface.api_version,
            interfaces.externalkeystonedeployment_interface(
                ctx.api_client).api_version
        )
        self.assertEquals(
            interface.plural,
            interfaces.externalkeystonedeployment_interface(
                ctx.api_client).plural
        )

    def test_get_resource_interface_raises(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "keystoneRef": {"kind": "foobar"},
        }
        self.assertRaises(
            ValueError,
            self.kr.get_resource_interface,
            ctx
        )

    def test_extracts_from_spec(self):
        ctx = unittest.mock.Mock()
        ctx.parent_spec = {
            "keystoneRef": {"name": sentinel.keystone_name},
        }

        self.assertEqual(
            self.kr.get_resource_references(ctx),
            [
                kclient.V1ObjectReference(namespace=ctx.namespace,
                                          name=sentinel.keystone_name),
            ],
        )


class TestExternalSecretReference(unittest.IsolatedAsyncioTestCase):
    async def test_get_with_external_secret_returns_none(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")

        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: None,
            secret_reference=service
        )
        result = await esr.get(sentinel.ctx)
        self.assertEqual(result.name, "servicename")

    async def test_get_with_external_secret_returns_not_none(self):
        service = unittest.mock.Mock()
        service.get = unittest.mock.AsyncMock()
        service.get.return_value = kclient.V1ObjectReference(
            name="servicename")

        sentinel.ctx.parent_spec = unittest.mock.Mock()
        ctx = unittest.mock.Mock(["parent_spec", "namespace"])
        ctx.parent_spec = {
            "api": {
                "ingress": {
                    "externalCertificateSecretRef": {
                        "name": "littleTestSecret"
                        }
                    }
                }
            }
        ctx.namespace = "myFancyNamespace"

        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: (
                ctx.parent_spec["api"]["ingress"]
                .get("externalCertificateSecretRef", {}).get("name")
            ),
            secret_reference=service
        )
        result = await esr.get(ctx)
        self.assertEqual(result.name, "littleTestSecret")

    async def test_delete_without_external_is_most_likely_noop(self):
        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: None,
            secret_reference=sentinel.secret,
        )
        await esr.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_delete_with_external_is_most_likely_noop(self):
        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: sentinel.external,
            secret_reference=sentinel.secret,
        )
        await esr.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_without_external_is_most_likely_noop(self):
        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: None,
            secret_reference=sentinel.secret,
        )
        await esr.cleanup_orphans(sentinel.ctx, sentinel.protect)

    async def test_cleanup_orphans_with_external_is_most_likely_noop(self):
        esr = dependencies.ExternalSecretReference(
            external_secret=lambda ctx: sentinel.external,
            secret_reference=sentinel.secret,
        )
        await esr.cleanup_orphans(sentinel.ctx, sentinel.protect)
