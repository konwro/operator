#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest
import unittest.mock

import yaook.common.ca

EXAMPLE_CERTIFICATE = """-----BEGIN CERTIFICATE-----
MIIFZTCCA02gAwIBAgIUVZtvTblE3gwv5o6+OaEHdun9OBQwDQYJKoZIhvcNAQEL
BQAwQjELMAkGA1UEBhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UE
CgwTRGVmYXVsdCBDb21wYW55IEx0ZDAeFw0yMTAxMDQwOTUyMTVaFw0yMjAxMDQw
OTUyMTVaMEIxCzAJBgNVBAYTAlhYMRUwEwYDVQQHDAxEZWZhdWx0IENpdHkxHDAa
BgNVBAoME0RlZmF1bHQgQ29tcGFueSBMdGQwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQDF40f+Vs3KdMZbOKez+7BagAZfd7OjDeFFcRpNvFoc+S3qJh/b
67/w7KuehSkI024CBLRO4nBZW+WqbLgbs11bW4bpqeiw3rxxIn7ekKIUsLE0dkQ3
t1UnQmesPtYTZop6fcy4RDqgK5nHk/cjyD3TtiJTxzF/8gbzA1Z96yxq5xtmn8fp
Hs9pDZJERcrqxi92SfGzF02ScI+YCSQIcJkwixKHfdX7ycFITtDcIdcIskc9QI1G
2rv3W729D/cCH4xJNtKqEDQlmZnUYyKwr+Kftx6wjtzw1Tcx+c5TvlBDd2ZbapS4
I2SLgXdDayz23mtkhBv9ydRhivxqv1AtmH87gxBcR1pQoz1VLD8viTNmlgZXmexY
wwI+Uql/qO2H1CrhPq0SFGLToD41UysNiRXdD3FwRHqVtmRzFj4cKWw1FHTM4H+Z
yN+CagOu96AvfN3++VtLBNFs71ypMwcvEkrEjs8lrJf28i27w5BAyELfMj6jq6QD
x7EozL7TWDc2KEFzZSwVPgeEJo37j3qgZwZJzndVAFNSfmEsRDZ80aypjnhCOBpI
SEhROCI63IUzE0RrBLczh13pgWIt1w5rZERpBItALaFAG5RHvDcCo06gAnZMrk8z
Wc3pwBzeJz2L8n6LDTMO3KF+EHKB4BOSXOesewJdBl9H/P6aGGzsxwKObQIDAQAB
o1MwUTAdBgNVHQ4EFgQUW4Cy7rOBKWgkN8df9t3Y5p6IJsMwHwYDVR0jBBgwFoAU
W4Cy7rOBKWgkN8df9t3Y5p6IJsMwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B
AQsFAAOCAgEAcYeV5rfAK+m+X4MRbMyEHcFmhDPD4k5YB9z/YQ/rkrcEZMTpVzEf
Nzy0Zr6e/x5Op8ZHzxeovPSREQdhfd06p+jSXDO3bOu9QiePL+nHSe8ujO8HkFnT
BzuEscmN4ITmSeiUJwaBNGgE38wq3a4sluhsbWtBlJSM7g17RKniW80wZ9tzH8gS
rLWGxNjSGVk/WI0TM9Xdc0Zi8JgrzJLsdsAuBtwaH25SLnlha1DCgLzQFgvX7CpG
QsHDvT034tpkcD+ldiRx2XZ8gutQvauCQQSvsAxEJAr51QX764thT4t/l4Qy2I/8
s/BvJ0KndTcb3i0t3gyv6kLnlgX68Y4lwkROUd4KU0jQg2YfziSBJtPkk7KlT0MO
BFRxJsZK5toAYp5LN2KXxP7Yq/mAlWsEt/9AvWc2Dx1iLXNil5AqBue2phCY4iSU
ZJGFuPAmIcOPjchTrV8f0ErLlTtBtur62v994X/bpY0EpuywXf9ebjISyKZ4BeZi
GFr1v5YVh2wX8alNHPgnu1VJkuIAGNrSgAY0LwGr0Y0SEvnrmJzreCW3lwf42hsd
OEWbPpAJDON39uRwWK8PmwuxISovyFM3ORRFdu2Nlw676ua8tS6DqNfvkQVHtgu8
y29QiKpnk5obrRbkoJI219QsW+L8Mc8Ijf2M/a+WXlirsbAhjy/JKD0=
-----END CERTIFICATE-----"""


EXAMPLE_INVALID_CERTIFICATE = """-----BEGIN CERTIFICATE-----
MIIFZTCCA02gAwIBAgIUVZtvTblE3gwv5o6+OaEHdun9OBQwDQYJKoZIhvcNAQEL
BQAwQjELMAkGA1UEBhMCWFgxFTATBgNVBAcMDERlZmF1bHQgQ2l0eTEcMBoGA1UE
CgwTRGVmYXVsdCBDb21wYW55IEx0ZDAeFw0yMTAxMDQwOTUyMTVaFw0yMjAxMDQw
OTUyMTVaMEIxCzAJBgNVBAYTAlhYMRUwEwYDVQQHDAxEZWZhdWx0IENpdHkxHDAa
BgNVBAoME0RlZmF1bHQgQ29tcGFueSBMdGQwggIiMA0GCSqGSIb3DQEBAQUAA4IC
DwAwggIKAoICAQDF40f+Vs3KdMZbOKez+7BagAZfd7OjDeFFcRpNvFoc+S3qJh/b
67/w7KuehSkI024CBLRO4nBZW+WqbLgbs11bW4bpqeiw3rxxIn7ekKIUsLE0dkQ3
t1UnQmesPtYTZop6fcy4RDqgK5nHk/cjyD3TtiJTxzF/8gbzA1Z96yxq5xtmn8fp
Hs9pDZJERcrqxi92SfGzF02ScI+YCSQIcJkwixKHfdX7ycFITtDcIdcIskc9QI1G
2rv3W729D/cCH4xJNtKqEDQlmZnUYyKwr+Kftx6wjtzw1Tcx+c5TvlBDd2ZbapS4
I2SLgXdDayz23mtkhBv9ydRhivxqv1AtmH87gxBcR1pQoz1VLD8viTNmlgZXmexY
wwI+Uql/qO2H1CrhPq0SFGLToD41UysNiRXdD3FwRHqVtmRzFj4cKWw1FHTM4H+Z
yN+CagOu96AvfN3++VtLBNFs71ypMwcvEkrEjs8lrJf28i27w5BAyELfMj6jq6QD
x7EozL7TWDc2KEFzZSwVPgeEJo37j3qgZwZJzndVAFNSfmEsRDZ80aypjnhCOBpI
SEhROCI63IUzE0RrBLczh13pgWIt1w5rZERpBItALaFAG5RHvDcCo06gAnZMrk8z
Wc3pwBzeJz2L8n6LDTMO3KF+EHKB4BOSXOesewJdBl9H/P6aGGzsxwKObQIDAQAB
o1MwUTAdBgNVHQ4EFgQUW4Cy7rOBKWgkN8df9t3Y5p6IJsMwHwYDVR0jBBgwFoAU
W4Cy7rOBKWgkN8df9t3Y5p6IJsMwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0B
AQsFAAOCAgEAcYeV5rfAK+m+X4MRbMyEHcFmhDPD4k5YB9z/YQ/rkrcEZMTpVzEf
Nzy0Zr6e/x5Op8ZHzxeovPSREQdhfd06p+jSXDO3bOu9QiePL+nHSe8ujO8HkFnT
BzuEscmN4ITmSeiUJwaBNGgE38wq3a4sluhsbWtBlJSM7g17RKniW80wZ9tzH8gS
rLWGxNjSGVk/WI0TM9Xdc0Zi8JgrzJLsdsAuBtwaH25SLnlha1DCgLzQFgvX7CpG
QsHDvT034tpkcD+ldiRx2XZ8gutQvauCQQSvsAxEJAr51QX764thT4t/l4Qy2I/8
s/BvJ0KndTcb3i0t3gyv6kLnlgX68Y4lwkROUd4KU0jQg2YfziSBJtPkk7KlT0MO
BFRxJsZK5toAYp5LN2KXxP7Yq/mAlWsEt/9AvWc2Dx1iLXNil5AqBue2phCY4iSU
ZJGFuPAmIcOPjchTrV8f0ErLlTtBtur62v994X/bpY0EpuywXf9ebjISyKZ4BeZi
GFr1v5YVh2wX8alNHPgnu1VJkuIAGNrSgAY0LwGr0Y0SEvnrmJzreCW3lwf42hsd
OEWbPpAJDON39uRwWK8PmwuxISovyFM3ORRFdu2Nlw676ua8tS6DqNfvkQVHtgu8
y29QiKpnk5obrRbkoJI219QsW+L8Mc8Ijf2M/a+WXlirsbAhjy/JKD0
-----END CERTIFICATE-----"""

EXAMPLE_CERTIFICATE_HASH = "9741086f.0"


class TestBuildCAConfigMap(unittest.TestCase):
    def test_all_certs_in_bundle(self):
        cfg = yaook.common.ca.build_ca_configmap()
        self.assertIn("ca-bundle.crt", cfg)
        for ca_key, cert in cfg.items():
            if ca_key == "ca-bundle.crt":
                continue
            self.assertIn(cert, cfg["ca-bundle.crt"])

    def test_adds_usercert(self):
        cfg = yaook.common.ca.build_ca_configmap(set([EXAMPLE_CERTIFICATE]))
        self.assertIn(EXAMPLE_CERTIFICATE, cfg["ca-bundle.crt"])
        self.assertIn(EXAMPLE_CERTIFICATE_HASH, cfg)
        self.assertEqual(EXAMPLE_CERTIFICATE, cfg[EXAMPLE_CERTIFICATE_HASH])

    def test_add_broken_cert(self):
        with self.assertRaises(ValueError):
            yaook.common.ca.build_ca_configmap(set(
                [EXAMPLE_INVALID_CERTIFICATE]))

    @unittest.mock.patch("OpenSSL.crypto.load_certificate")
    def test_duplicate_ca_hash(self, load_certificate):
        def mock_load_certificate(filetype, content):
            mock = unittest.mock.Mock(["subject_name_hash"])
            if content.startswith("ca_cert"):
                mock.subject_name_hash.return_value = 0xABCDEF
            else:
                mock.subject_name_hash.return_value = 0x12345
            return mock

        load_certificate.side_effect = mock_load_certificate
        cfg = yaook.common.ca.build_ca_configmap(set(["ca_cert1", "ca_cert2"]))
        self.assertIn("abcdef.0", cfg)
        self.assertIn("abcdef.1", cfg)
        # ordering must be stable based on the certificate pem string
        # this is to ensure we do not wrongly update the ca certificates
        # configmap
        self.assertEqual(cfg["abcdef.0"], "ca_cert1")
        self.assertEqual(cfg["abcdef.1"], "ca_cert2")
