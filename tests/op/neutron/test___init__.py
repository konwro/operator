import ddt
import logging
import unittest
import unittest.mock

from yaook.op.neutron import Neutron
import yaook.statemachine.exceptions as exceptions


@ddt.ddt()
class TestNeutronCustomResourceDefinition(unittest.IsolatedAsyncioTestCase):
    class NeutronTest(Neutron):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "neutrondeployments"
        KIND = "NeutronDeployment"
        RELEASES = ["train",
                    "yoga",
                    "zed",
                    "2023.1",
                    ]
        VALID_UPGRADE_TARGETS = ["zed",
                                 "2023.1",
                                 ]

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)
        self.sm = unittest.mock.Mock(["ensure"])
        self.sm.ensure = unittest.mock.AsyncMock()

        self.ctx = unittest.mock.Mock(["logger"])
        self.cr = self.NeutronTest(logger=self.logger)

    async def test__validate_config_raises_wrong_bgp_config(self):
        self.ctx.parent = {
            "spec": {
                "targetRelease": "train",
                "setup": {
                    "ovs": {
                        "bgp": {
                            "WrongBGPConfigKeys": {
                                "configTemplates": [
                                    {
                                        "nodeSelectors": [
                                            {"matchLabels": {}},  # all nodes
                                        ],
                                        "neutronConfig": {},
                                        "neutronBGPDragentConfig": {
                                            "DEFAULT": {
                                                "use_stderr": True,
                                            },
                                        },
                                    },
                                ],
                            }
                        },
                    }
                }
            }
        }
        self.ctx.parent_spec = self.ctx.parent["spec"]
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "Configkeys for the BGPDRAgents need to be lowercase"):
            await self.cr._validate_config(self.ctx)

    @ddt.data("train")
    async def test__validate_config_raises_wrong_setup_ovn(self, release):
        self.ctx.parent = {
            "spec": {
                "targetRelease": release,
                "setup": {
                    "ovn": {}
                },
            }
        }
        self.ctx.parent_spec = self.ctx.parent["spec"]
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "ovn is not supported for openstack release " + release):
            await self.cr._validate_config(self.ctx)

    async def test__validate_config_raises_wrong_setup_ovs(self):
        self.ctx.parent = {
            "spec": {
                "targetRelease": "yoga",
                "setup": {
                    "ovs": {}
                },
            }
        }
        self.ctx.parent_spec = self.ctx.parent["spec"]
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "ovs is not supported for openstack release yoga"):
            await self.cr._validate_config(self.ctx)
