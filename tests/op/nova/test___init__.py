#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import typing
import ddt
import logging
import unittest
import unittest.mock

from yaook.op.nova import Nova
import yaook.statemachine.exceptions as exceptions


@ddt.ddt()
class TestNovaCustomResourceDefinition(unittest.IsolatedAsyncioTestCase):
    class NovaTest(Nova):
        API_GROUP = "yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "novadeployments"
        KIND = "NovaDeployment"
        RELEASES = [
            "train",
            "yoga",
            "zed",
        ]
        VALID_UPGRADE_TARGETS: typing.List[str] = ["zed"]

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)
        self.sm = unittest.mock.Mock(["ensure"])
        self.sm.ensure = unittest.mock.AsyncMock()
        self._env_backup = os.environ.pop(
            "YAOOK_EVICT_MANAGER_IMAGE", None
        )
        os.environ["YAOOK_EVICT_MANAGER_IMAGE"] = "dummy-image"
        self.ctx = unittest.mock.Mock(["logger"])
        self.cr = self.NovaTest(logger=self.logger)

    async def test__validate_config_raises_diffrent_timeout_clients(self):
        timeoutCell0 = 180
        timeoutCell1 = 300
        timeoutApi = 300
        self.ctx.parent = {
            "spec": {
                "targetRelease": "zed",
                "database": {
                    "cell0": {
                        "timeoutClient": timeoutCell0
                    },
                    "cell1": {
                        "timeoutClient": timeoutCell1
                    },
                    "api": {
                        "timeoutClient": timeoutApi
                    }
                }
            }
        }
        self.ctx.parent_spec = self.ctx.parent["spec"]
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "Multiple values for the cell0, cell1 and api database "
                "timeoutclient specified \(cell0\): "  # noqa: W605
                rf"{timeoutCell0} \(cell1\): {timeoutCell1} "  # noqa: W605,E501
                rf"\(api\): {timeoutApi}"):  # noqa: W605,E501
            await self.cr._validate_config(self.ctx)
