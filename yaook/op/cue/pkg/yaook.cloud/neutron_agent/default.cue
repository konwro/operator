// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package neutron_agent

import (
	"strings"
	"yaook.cloud/neutron_template"
)

neutron_agent_conf_spec: neutron_template.neutron_template_conf_spec
neutron_agent_conf_spec: {
	DEFAULT: {
		#transport_url_hosts:    *["rabbitmq-neutron"] | [...string]
		#transport_url_port:     *5671 | int
		#transport_url_username: *"admin" | string
		#transport_url_password: *"" | string
		#transport_url_vhost:    *"" | string
		#transport_url_parts: [ for Host in #transport_url_hosts {"\( #transport_url_username ):\( #transport_url_password )@\( Host ):\( #transport_url_port )"}]
		if #transport_url_password != "" {
			transport_url: "rabbit://\( strings.Join(#transport_url_parts, ",") )/\( #transport_url_vhost )"
		}
		use_stderr:            *true | bool
		use_json:              *true | bool
		metadata_proxy_socket: "/run/neutron/metadata_proxy_socket"
	}
	oslo_concurrency: {
		"lock_path": "/var/lib/neutron/tmp"
	}
	if DEFAULT.#transport_url_password != "" {
		oslo_messaging_rabbit: {
			ssl:                 true
			ssl_ca_file:         "/etc/pki/tls/certs/ca-bundle.crt"
			amqp_durable_queues: true
			rabbit_quorum_queue: true
		}
	}
	privsep: {
		helper_command: "sudo /usr/local/bin/privsep-helper --config-file /etc/neutron/neutron.conf"
	}
}
