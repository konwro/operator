package mariadb_template

mariadb_common_conf_spec: {
	"plugin_dir"?: string
	"port"?:       int
	"socket"?:     string

	// SSL
	"ssl"?:        null
	"ssl_ca"?:     string
	"ssl_cert"?:   string
	"ssl_key"?:    string
	"ssl_cipher"?: string
}

mariadb_client_conf_spec: mariadb_common_conf_spec

mariadb_server_conf_spec: mariadb_common_conf_spec
mariadb_server_conf_spec: {
	"default_storage_engine"?: string
	"basedir"?:                string
	"datadir"?:                string
	"tmpdir"?:                 string
	"pid_file"?:               string
	"collation_server"?:       string
	"init_connect"?:           string
	"character_set_server"?:   string

	// MyISAM
	"key_buffer_size"?:        =~"^[0-9]+[MkG]B?$"
	"myisam_recover_options"?: string

	// Safety
	"skip_host_cache"?:    null
	"skip_name_resolve"?:  null
	"max_allowed_packet"?: =~"^[0-9]+[MkG]B?$"
	"sql_mode"?:           string
	"sysdate_is_now"?:     bool

	// Binary logging
	"log_bin"?:          string
	"expire_logs_days"?: int
	"sync_binlog"?:      bool
	"binlog_format"?:    "row" | "statement" | "mixed"

	// Caches and limits
	"tmp_table_size"?:         =~"^[0-9]+[MkG]B?$"
	"max_heap_table_size"?:    =~"^[0-9]+[MkG]B?$"
	"query_cache_type"?:       bool | 2 | "demand"
	"query_cache_limit"?:      =~"^[0-9]+[MkG]B?$"
	"query_cache_size"?:       =~"^[0-9]+[MkG]B?$"
	"max_connections"?:        int
	"thread_cache_size"?:      int
	"open_files_limit"?:       int
	"table_definition_cache"?: int
	"table_open_cache"?:       int

	// InnoDB
	"innodb"?:                         "on" | "off" | "force" | "force_plus_permanent"
	"innodb_strict_mode"?:             bool
	"innodb_autoinc_lock_mode"?:       0 | 1 | 2
	"innodb_doublewrite"?:             bool
	"innodb_flush_method"?:            "fsync" | "O_DSYNC" | "O_DIRECT" | "O_DIRECT_NO_FSYNC" | "ALL_O_DIRECT"
	"innodb_log_files_in_group"?:      >=2 & <=100
	"innodb_log_file_size"?:           =~"^[0-9]+[MkG]B?$"
	"innodb_flush_log_at_trx_commit"?: 0 | 1 | 2 | 3
	"innodb_file_per_table"?:          bool
	"innodb_buffer_pool_size"?:        =~"^[0-9]+[MkG]B?$"
	"innodb_file_format"?:             "Antelope" | "Barracuda"
	"innodb_data_file_buffering"?:     bool
	"innodb_log_file_buffering"?:      bool
	"innodb_data_file_write_through"?: bool
	"innodb_log_file_write_through"?:  bool

	// Logging
	"log_error"?:                     string
	"slow_query_log_file"?:           string
	"log_queries_not_using_indexes"?: bool
	"slow_query_log"?:                bool

	// Misc
	"plugin_load_add"?: string
}

mariadb_galera_conf_spec: mariadb_server_conf_spec
mariadb_galera_conf_spec: {
	"wsrep_on"?:               bool
	"wsrep_provider"?:         string
	"wsrep_sst_method"?:       "rsync" | "mysqldump" | "xtrabackup" | "xtrabackup-v2" | "mariabackup"
	"wsrep_slave_threads"?:    int
	"wsrep_cluster_address"?:  string
	"wsrep_cluster_name"?:     string
	"wsrep_sst_auth"?:         string
	"wsrep_replicate_myisam"?: bool
	"wsrep_mode"?:             "BINLOG_ROW_FORMAT_ONLY" | "DISALLOW_LOCAL_GTID" | "REQUIRED_PRIMARY_KEY" | "REPLICATE_ARIA" | "REPLICATE_MYISAM" | "STRICT_REPLICATION"
	"wsrep_provider_options"?: {
		"socket.ssl"?:             "yes" | "no"
		"socket.ssl_ca"?:          string
		"socket.ssl_cert"?:        string
		"socket.ssl_cipher"?:      string
		"socket.ssl_compression"?: "yes" | "no"
		"socket.ssl_key"?:         string
		"gcomm.thread_prio"?:      =~"^(fifo|rr|other):[0-9]+$"
		"gcs.fc_limit"?:           int
		"gcs.fc_factor"?:          float
		"gcs.fc_master_slave"?:    "yes" | "no"
		"gcs.max_packet_size"?:    int
		"gcache.size"?:            string
	}
}

mariadb_template_conf_spec: {
	"client-server"?: mariadb_server_conf_spec
	"client-server"?: mariadb_client_conf_spec
	"client"?:        mariadb_common_conf_spec
	"mysqld"?:        mariadb_server_conf_spec
	"galera"?:        mariadb_galera_conf_spec
	"sst"?: {
		"encrypt"?: 2 | 3 | 4
		"tkey"?:    string
		"tcert"?:   string
		"tca"?:     string
	}
}
