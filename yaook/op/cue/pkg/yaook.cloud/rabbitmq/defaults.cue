// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rabbitmq

import (
	"yaook.cloud/rabbitmq_template"
)

rabbitmq_conf_spec: rabbitmq_template.rabbitmq_template_conf_spec
rabbitmq_conf_spec: {
	#ssl_ca_filename:   *"ca.crt" | string
	#ssl_key_filename:  *"tls.key" | string
	#ssl_cert_filename: *"tls.crt" | string
	#ssl_dir:           *"/etc/rabbitmq/certs/frontend" | string
	#ssl_ca:            *"\(#ssl_dir)/\(#ssl_ca_filename)" | string
	#ssl_cert:          *"\(#ssl_dir)/\(#ssl_cert_filename)" | string
	#ssl_key:           *"\(#ssl_dir)/\(#ssl_key_filename)" | string

	"cluster_formation": {
		"peer_discovery_backend":        "rabbit_peer_discovery_k8s"
		"k8s.host":                      *"kubernetes.default.svc.cluster.local" | string
		"node_cleanup.interval":         *10 | uint64
		"node_cleanup.only_log_warning": *true | bool
	}
	"cluster_partition_handling": *"autoheal" | string
	"queue_master_locator":       *"client-local" | string
	"loopback_users": {"guest": false}
	"listeners": {
		"tcp": "none"
		"ssl": [5671]
	}
	"ssl_options": {
		"cacertfile": #ssl_ca
		"certfile":   #ssl_cert
		"keyfile":    #ssl_key
	}
	"management": {
		"tcp": {
			"ip": "127.0.0.1"
		}
		"ssl": {
			"port":       15671
			"cacertfile": #ssl_ca
			"certfile":   #ssl_cert
			"keyfile":    #ssl_key
		}
	}
	"prometheus": {
		"ssl": {
			"port":       15691
			"cacertfile": #ssl_ca
			"certfile":   #ssl_cert
			"keyfile":    #ssl_key
		}
	}
	"net_ticktime": *10 | uint64
}
