package rabbitmq_template

#rabbitmq_bytes_spec: uint64 | =~"^[0-9]+[GMk]i?B$"

#rabbitmq_watermark_spec: {
	"relative": float
} | {
	"absolute": #rabbitmq_bytes_spec
}

rabbitmq_common_ssl_conf_spec: {
	"cacertfile"?:           string
	"certfile"?:             string
	"keyfile"?:              string
	"honor_cipher_order"?:   bool
	"honor_ecc_order"?:      bool
	"client_renegotiation"?: bool
	"secure_renegotiate"?:   bool
	"ciphers"?: [... string]
}

rabbitmq_template_conf_spec: {
	"listeners"?: {
		"tcp"?: [... string | uint16] | "none"
		"ssl"?: [... string | uint16] | "none"
	}
	"num_acceptors"?: {
		"tcp"?: uint32
		"ssl"?: uint32
	}
	"socket_writer"?: {
		"gc_threshold": uint64 | "off"
	}
	"handshake_timeout"?:   uint64
	"reverse_dns_lookups"?: bool
	"loopback_users"?: {
		"guest"?: bool
	}
	"ssl_options"?: rabbitmq_common_ssl_conf_spec
	"ssl_options"?: {
		"verify"?:               bool
		"fail_if_no_peer_cert"?: bool
		"bypass_pem_cache"?:     bool
	}
	"auth_backends"?: [... string | {
		"authn": string
		"authz": string
	}]
	"auth_mechanisms"?: [... string]
	"ssl_cert_login_from"?:     string
	"ssl_handshake_timeout"?:   uint64
	"cluster_name"?:            string
	"password_hashing_module"?: string
	"default_vhost"?:           string
	"default_user"?:            string
	"default_pass"?:            string
	"default_permissions"?: {
		"configure"?: string
		"read"?:      string
		"write"?:     string
	}
	"default_user_tags"?: [... string]
	"heartbeat"?:         uint64
	"frame_max"?:         uint64
	"initial_frame_max"?: uint64
	"channel_max"?:       uint64
	"tcp_listen_options"?: {
		"backlog"?:       uint32
		"nodelay"?:       bool
		"exit_on_close"?: bool
		"keepalive"?:     bool
		"send_timeout"?:  uint64
		"buffer"?:        uint64
		"sndbuf"?:        uint64
		"recbuf"?:        uint64
	}
	"vm_memory_high_watermark"?:              #rabbitmq_watermark_spec
	"vm_memory_high_watermark_paging_ratio"?: float
	"vm_memory_calculation_strategy"?:        "allocated" | "rss" | "legacy"
	"memory_monitor_interval"?:               uint64
	"total_memory_available_override_value"?: #rabbitmq_bytes_spec
	"disk_free_limit"?:                       #rabbitmq_watermark_spec
	"cluster_partition_handling"?:            "ignore" | "pause-if-all-down" | "pause-minority" | "autoheal"
	"mirroring_sync_batch_size"?:             uint64
	"cluster_formation"?: {
		"peer_discovery_backend"?: string
		"node_type"?:              "disc" | "ram"
	}
	"cluster_keepalive_interval"?:  uint64
	"collect_statistics_interval"?: uint64
	"raft"?: {
		"segment_max_entries"?: uint64
		"wal_max_size_bytes"?:  uint64
		"wal_max_batch_size"?:  uint64
		"snapshot_chunk_size"?: uint64
	}
	"net_ticktime"?: uint64
	"management"?: {
		"load_definitions"?: string
		"http_log_dir"?:     string
		"tcp"?:              {
			"port"?:               uint16
			"ip"?:                 string
			"shutdown_timeout"?:   uint64
			"max_keepalive"?:      uint64
			"idle_timeout"?:       uint64
			"inactivity_timeout"?: uint64
			"compress"?:           bool
		} | "none"
		"ssl"?: rabbitmq_common_ssl_conf_spec
		"ssl"?: {
			"port"?: uint16
		}
		"path_prefix"?: string
		"rates_mode"?:  "none" | "detailed" | "basic"
	}
}
