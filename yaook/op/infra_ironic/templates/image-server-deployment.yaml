##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ "%s-imgserv" | format(labels['state.yaook.cloud/parent-name']) }}
  annotations:
    test.yaook.cloud/skip-ca-certs: "true"
spec:
  # currently, we cannot easily support more replicas because we only have a
  # ReadWriteOnce pvc
  replicas: 1
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.api.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      initContainers:
      - name: bootstrap-cfg
        image: debian:bullseye
        command:
        - bash
        - -euo
        - pipefail
        - -c
        - |
          mkdir -p /tmp/http-data/pxelinux.cfg
          # Fallback from boot.ipxe
          cp /tmp/boot-scripts/default-x86.ipxe /tmp/http-data/pxelinux.cfg/default-x86
          cp /tmp/boot-scripts/default-arm64.ipxe /tmp/http-data/pxelinux.cfg/default-arm64
          # We need to deploy the default script so that something exists before the conductor kicks in and templates boot.ipxe to that location.
          cp --no-clobber /tmp/boot-scripts/default-x86.ipxe /tmp/http-data/boot-x86.ipxe
          cp --no-clobber /tmp/boot-scripts/default-arm64.ipxe /tmp/http-data/boot-arm64.ipxe
          # Also we want to copy the deploy scripts into the http root folder.
        volumeMounts:
        - name: http-data
          mountPath: /tmp/http-data
        - name: boot-scripts
          mountPath: /tmp/boot-scripts
        resources: {{ crd_spec | resources('imageServer.bootstrap-cfg') }}
      containers:
      - name: httpd
        image: {{ versioned_dependencies["nginx_image"] }}
        volumeMounts:
        - name: http-data
          mountPath: /usr/share/nginx/html
        ports:
        - name: http
          containerPort: 80
        resources: {{ crd_spec | resources('imageServer.httpd') }}
      volumes:
      - name: http-data
        persistentVolumeClaim:
          claimName: {{ dependencies['data_volume'].resource_name() }}
      - name: boot-scripts
        configMap:
          name: {{ dependencies['bootscripts'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
