#
# Copyright (c) 2024 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import dataclasses
import time
import typing
import environ

import yaook.statemachine.commoneviction as eviction

from ovsdbapp.schema.ovn_northbound import impl_idl as nb_impl
from ovsdbapp.schema.ovn_southbound import impl_idl as sb_impl
from ovsdbapp.backend.ovs_idl.idlutils import RowNotFound
from ovsdbapp.backend.ovs_idl import connection
from ovs import stream


@dataclasses.dataclass(frozen=True)
class EvictionStatus:
    migrated_lrps: int
    unmigratable_lrps: int
    unhandleable_gateway: bool

    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        return {
            "migrated": self.migrated_lrps,
            "unmigratable": self.unmigratable_lrps,
            "unhandleable_gateway": self.unhandleable_gateway,
        }


@environ.config(prefix="YAOOK_OP_OVN_EVICTION")
class OvnEvictionConfig:
    commit_sleep_time = environ.var(converter=int, default=10)
    db_connection_timeout = environ.var(converter=int, default=150)
    probe_interval = environ.var(converter=int, default=0)


class Evictor(eviction.Evictor[EvictionStatus]):
    def __init__(
        self,
        nb_db: str,
        sb_db: str,
        **kwargs: typing.Any,
    ):
        super().__init__(**kwargs)
        config = environ.to_config(OvnEvictionConfig)
        self.commit_sleep_time = config.commit_sleep_time
        self.db_connection_timeout = config.db_connection_timeout
        self.probe_interval = config.probe_interval
        self.nb_api, self.sb_api = self._connect(nb_db, sb_db)
        self.updates: typing.List[typing.Tuple[str, int, str, str]] = []
        self.unmigratable_lrps: typing.Set[str] = set()
        self.priorities: typing.List[int] = []

        # Place LRPs for the routers to a datastructure of the following type
        # {'<gtw-name>': {<priority>:[<lrp-obj>, <lrp-obj>, ...]}}
        # {'gtw01': {1:[lrp-xxx, lrp-yyy]}}
        self.current_dist: typing.Dict[str, typing.Dict[
            int, typing.List[str]]] = {}

    def _connect(
        self,
        nb_db: str,
        sb_db: str,
      ) -> tuple[nb_impl.OvnNbApiIdlImpl, sb_impl.OvnSbApiIdlImpl]:
        stream.Stream.ssl_set_private_key_file("/etc/ssl/private/tls.key")
        stream.Stream.ssl_set_certificate_file("/etc/ssl/private/tls.crt")
        stream.Stream.ssl_set_ca_cert_file("/etc/ssl/private/ca.crt")
        self._logger.info("Connecting to the OVN DBs")
        try:
            nb_idl = connection.OvsdbIdl.from_server(
                nb_db, "OVN_Northbound", leader_only=False,
                probe_interval=self.probe_interval
            )
            nb_conn = connection.Connection(idl=nb_idl,
                                            timeout=self.db_connection_timeout)
            nb_api = nb_impl.OvnNbApiIdlImpl(nb_conn)

            sb_idl = connection.OvsdbIdl.from_server(
                sb_db, "OVN_Southbound", leader_only=False,
                probe_interval=self.probe_interval
            )
            sb_conn = connection.Connection(idl=sb_idl,
                                            timeout=self.db_connection_timeout)
            sb_api = sb_impl.OvnSbApiIdlImpl(sb_conn)
        except Exception as e:
            raise Exception(
                "Cannot connect to OVS DBs, we cannot do anything, "
                "as we cannot get the of logical router ports of the "
                f"gateway chassis {self._node_name}: {e}."
            )
        self._logger.info("Connected to the OVN DBs")
        return (nb_api, sb_api)

    def search_for_chassis(self, priority: int) -> typing.List[str]:
        chassis_lrp_count = {}
        for chassis in self.active_chassis:
            if not chassis == self._node_name:
                chassis_lrp_count[chassis] = \
                    len(self.current_dist[chassis][priority])

        return sorted(chassis_lrp_count,
                      key=lambda chassis: chassis_lrp_count[chassis])

    def update_priorities(self, old_chassis: str, new_chassis: str,
                          lrp: str,
                          prio_to_be_migrated: int) -> bool:
        for prio in self.priorities:
            # Check if we have the same LRP with another priority on
            # the new gateway
            if lrp in self.current_dist[new_chassis][prio]:
                return False

        self.current_dist[old_chassis][prio_to_be_migrated].remove(lrp)
        self.current_dist[new_chassis][prio_to_be_migrated].append(lrp)

        self.updates.append((lrp, prio_to_be_migrated, old_chassis,
                             new_chassis))
        return True

    def evict(self) -> None:
        # If we have less than 5 chassis just remove priority of the LRP from
        # the current chassis
        if len(self.active_chassis) <= 5:
            for priority in self.priorities:
                lrp_list = self.current_dist[self._node_name][priority].copy()
                for lrp in lrp_list:
                    self.current_dist[self._node_name][priority].remove(lrp)
                    self.updates.append((lrp, priority, self._node_name, ""))
        else:
            for priority in self.priorities:
                lrp_list = self.current_dist[self._node_name][priority].copy()
                for lrp in lrp_list:
                    candidate_chassis = self.search_for_chassis(priority)
                    for chassis in candidate_chassis:
                        if self.update_priorities(self._node_name,
                                                  chassis,
                                                  lrp, priority):
                            break

    def _init_lrp_distribution(
            self,
    ) -> None:
        self.priorities = list(range(1, 6)) if len(self.active_chassis) >= 5 \
            else list(range(1, len(self.active_chassis) + 1))

        for chassis in self.all_chassis:
            self.current_dist[chassis] = {x: [] for x in self.priorities}

    def _display_distribution(self) -> None:
        for priority in self.priorities:
            chassis_lrp_count = {}
            self._logger.info(f"Priority {priority}")
            for chassis in self.current_dist:
                chassis_lrp_count[chassis] = len(
                    self.current_dist[chassis][priority])
                self._logger.info(f"  {chassis} has "
                                  f"{chassis_lrp_count[chassis]} routers")

    def _fill_lrp_distribution(
            self,
    ) -> None:
        self._logger.info("Collecting LRPs distribution from OVN DB")
        lrps = self.nb_api.db_find_rows('Logical_Router_Port',
                                        ('gateway_chassis', '!=', [])
                                        ).execute()

        for lrp in lrps:
            for gc_row in lrp.gateway_chassis:
                if gc_row.priority not in self.priorities:
                    raise Exception("Invalid priority "
                                    f"{gc_row.priority} for LRP {lrp.name}, "
                                    "priorities should only be from 1 till "
                                    f"{len(self.priorities)}")
                self.current_dist[gc_row.chassis_name][
                    gc_row.priority].append(lrp.name)

        self._display_distribution()

        self._logger.info("Collected LRPs distribution")

    def _get_active_chassis(
        self,
    ) -> typing.Tuple[typing.List[str], typing.List[str]]:
        all_chassis: typing.List[str] = []
        active_chassis: typing.List[str] = []
        rows = self.nb_api.db_list_rows("NB_Global").execute()
        nb_cfg_ts_epoch = 0
        for row in rows:
            nb_cfg_ts_epoch = row.nb_cfg_timestamp

        if nb_cfg_ts_epoch == 0:
            raise Exception("nb_cfg_timestamp is wrong in the NB_Global. "
                            "Aborting")

        chassis_rows = self.sb_api.db_find_rows(
            "Chassis",
            (
                'other_config',
                '=',
                {'ovn-cms-options': 'enable-chassis-as-gw'}
            )
        ).execute()
        for row in chassis_rows:
            all_chassis.append(row.name)
            try:
                nb_cfg_timestamp = self.sb_api.db_get(
                    'Chassis_Private', row.name, 'nb_cfg_timestamp'
                ).execute()
                # 2 mins diff. is acceptable
                if nb_cfg_timestamp >= nb_cfg_ts_epoch-120000:
                    active_chassis.append(row.name)
                else:
                    self._logger.warning("`nb_cfg_timestamp` older then 2 mins"
                                         f" for {row.name}, not counting it as"
                                         "ACTIVE chassis")
            except RowNotFound:
                self._logger.warning("`nb_cfg_timestamp` not found for "
                                     f"{row.name} in `Chassis_Private` table")

        return (all_chassis, active_chassis)

    def _commit_updates(
        self,
        sleep: float
    ) -> bool:
        self._logger.info(f"Committing {len(self.updates)} updates to OVN DB")
        for _ in range(len(self.updates)):
            lrp, priority, old_gc, new_gc = self.updates.pop()
            try:
                with self.nb_api.transaction(check_error=True) as txn:
                    if new_gc == "":
                        self._logger.info(f"Removing LRP {lrp} from {old_gc}")
                        txn.add(self.nb_api.lrp_del_gateway_chassis(lrp,
                                                                    old_gc,
                                                                    priority))
                    else:
                        self._logger.info(f"Migrating priority {priority} for "
                                          f"LRP {lrp} from {old_gc} "
                                          f"to {new_gc}")
                        txn.add(self.nb_api.lrp_set_gateway_chassis(lrp,
                                                                    new_gc,
                                                                    priority))
                        txn.add(self.nb_api.lrp_del_gateway_chassis(lrp,
                                                                    old_gc,
                                                                    priority))

                if priority < 5:
                    # Higher priority higher sleep time b/w commits
                    time.sleep(sleep/(6-priority))
                else:
                    time.sleep(sleep)
            except Exception as e:
                self._logger.error(f"Failed to commit updates. Error: {e}")
                return False

        return True

    def _os_iterate(self) -> EvictionStatus:
        unhandleable_gateway = True
        migrated_lrps = 0
        self.all_chassis, self.active_chassis = self._get_active_chassis()

        self._init_lrp_distribution()
        self._fill_lrp_distribution()
        self.evict()
        migrated_lrps = len(self.updates)
        eviction_succeeded = self._commit_updates(self.commit_sleep_time)

        if not eviction_succeeded:
            self._logger.info(f"Failed to evict {self.updates} LRPs out of "
                              f"{migrated_lrps} from gateway chassis "
                              f"{self._node_name}")
        else:
            unhandleable_gateway = False
            self._logger.info(f"Eviction of {migrated_lrps} LRPs from gateway "
                              f"chassis {self._node_name} succeeded")

        return EvictionStatus(
            migrated_lrps=migrated_lrps,
            unmigratable_lrps=len(self.updates),
            unhandleable_gateway=unhandleable_gateway,
        )

    def _lrps_on_chassis(
        self,
        gateway_name: str,
    ) -> typing.List:
        chassis_info = self.nb_api.db_find(
            "Gateway_Chassis", ("chassis_name", "=", gateway_name)
        ).execute()
        return chassis_info

    async def is_migration_done(
        self,
        status: EvictionStatus,
    ) -> bool:
        remaining_chassis = self._lrps_on_chassis(self._node_name)

        return (
            len(remaining_chassis) == 0
            and not status.unhandleable_gateway
        )

    def start_eviction_log(
        self,
        poll_interval: int,
    ) -> str:
        return (
            f"initiating eviction of node {self._node_name} with reason "
            f"{self._reason}.\npoll interval is {poll_interval}s"
        )
