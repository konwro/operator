#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import typing

import openstack

import yaook.statemachine as sm
from yaook.statemachine import (
    context,
    customresource,
    watcher,
)
from yaook.statemachine.resources.base import DependencyMap
import yaook.statemachine.resources.openstack as resource
from yaook.statemachine.resources.openstack import ResourceStatus


class OVNStateResource(
    resource.APIStateResource,
    resource.L2ProvidingAgentStateResourceMixin,
):
    def __init__(
            self,
            **kwargs: typing.Any):
        super().__init__(**kwargs)
        self.neutronAgentWatch: watcher.ExternalWatcher[
                openstack.network.v2.agent.Agent] = resource.\
            NeutronAgentWatcher("NeutronOVNAgent", "ovn-controller")

    def get_listeners(self) -> typing.List[context.Listener]:
        return super().get_listeners() + [
            context.KubernetesListener[typing.Mapping](
                "", "v1", "nodes",
                self._handle_event,
                broadcast=True,
            ),
        ] + [
            context.ExternalListener(
                watcher=self.neutronAgentWatch,
                listener=self._handle_agent_event,
            ),
        ]

    def _get_agent(
            self,
            network_client: openstack.network.v2._proxy.Proxy,
            host: str,
            ) -> typing.Optional[openstack.network.v2.agent.Agent]:
        try:
            agent = resource.get_network_agent(network_client, host,
                                               "ovn-controller")
        except LookupError:
            return None
        return agent

    def _get_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            ) -> typing.Optional[resource.ResourceStatus]:
        with openstack.connect(**connection_info) as client:
            agent = self._check_agent(ctx, client)
            if agent is None:
                return None
            is_state_up = agent.is_admin_state_up
            ctx.logger.debug("is_state_up = %r", is_state_up)

            # As we did not raise by now the connection parameters seem to be
            # valid
            self.neutronAgentWatch.connection_parameters = connection_info
            self.neutronAgentWatch.namespace = ctx.namespace

            return resource.ResourceStatus(
                up=agent.is_alive,
                enabled=agent.is_admin_state_up,
                disable_reason=None,
            )

    def _get_resource(
            self,
            client: openstack.connection.Connection,
            ctx: context.Context
            ) -> typing.Optional[resource.ResourceStatus]:
        agent = self._check_agent(ctx, client)
        if agent is None:
            return None
        return resource.ResourceStatus(
            up=agent.is_alive,
            enabled=agent.is_admin_state_up,
            disable_reason=None,
        )

    def _update_status(
            self,
            ctx: context.Context,
            connection_info: typing.Mapping[str, typing.Any],
            enabled: bool,
            ) -> typing.Optional[ResourceStatus]:
        return None

    async def update(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        await super().update(ctx, dependencies)
        conn_info = await self.get_openstack_connection_info(ctx)
        loop = asyncio.get_event_loop()

        status = await loop.run_in_executor(
            None,
            self._get_status,
            ctx,
            conn_info,
        )
        ctx.logger.debug("read status from OpenStack: %s",
                         status)

        if status and status.up:
            await self._maintenance_required_label(
                ctx,
                "add",
                False,
                )

    async def _eviction_done(self, ctx: context.Context) -> bool:
        is_compute_node = ctx.parent["spec"].get(
            "deployedOnComputeNode", False)

        # This is only to rollout this eviction change seemlessly.
        # It should be removed after 3 months(May 2024), as by then *hopefully*
        # all gateways will have the northboundServers field in their spec
        is_gtw_without_nb_servers = "northboundServers" not in \
                                    ctx.parent["spec"]

        # Only evict OVN agents on gateway nodes
        return is_compute_node or is_gtw_without_nb_servers or \
            await self._background_job.is_ready(ctx)

    async def _background_job_reconcile(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        is_gateway_node = not ctx.parent["spec"].get(
            "deployedOnComputeNode", False)

        # This is only to rollout this eviction change seemlessly.
        # It should be removed after 3 months(May 2024), as by then *hopefully*
        # all gateways will have the northboundServers field in their spec
        is_gtw_without_nb_servers = "northboundServers" not in \
                                    ctx.parent["spec"]

        if is_gateway_node and not is_gtw_without_nb_servers:
            await self._background_job.reconcile(
                ctx,
                dependencies=dependencies
            )

    async def delete(
            self,
            ctx: context.Context,
            dependencies: DependencyMap,
            ) -> None:
        await super().delete(ctx, dependencies)
        await self.delete_resource(ctx, dependencies)


class OVNControllerStatefulSet(resource.TemplatedRecreatingStatefulSet):

    async def _update_ovn_version_status(self, ctx: context.Context) -> None:
        try:
            instance = await self._get_current(ctx)
        except sm.ResourceNotPresent:
            return

        ovn_url, = [container.image
                    for container in instance.spec.template.spec.containers
                    if container.name == "ovn-controller"]

        version_format = sm.OVNVersion()

        try:
            ovn_semver, _ = version_format.parse(ovn_url.split(':')[-1])
            ovn_version = version_format.serialize_software_build(ovn_semver)
        except ValueError:
            ctx.logger.error('No OVN version in image url: %s, '
                             'OVN upgrade path cannot be enforced!', ovn_url)
            return

        await customresource.update_status(
            ctx.parent_intf,
            ctx.namespace,
            ctx.parent_name,
            additional_patch={'ovnVersion': ovn_version}
        )

    async def reconcile(self,
                        ctx: context.Context,
                        **kwargs: typing.Any,
                        ) -> None:
        await super().reconcile(ctx, **kwargs)
        await self._update_ovn_version_status(ctx)
