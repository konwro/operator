#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.op.tasks` -- Management of recurring and resilient tasks
#####################################################################

.. autoclass:: ExponentialBackOff

.. autoclass:: TaskQueue

.. autoclass:: RestartingTask
"""

from __future__ import annotations
import asyncio
import dataclasses
import logging
import random
import time
import typing

import environ
from prometheus_client import Gauge


clock = time.monotonic
metric_task_queue_length = Gauge('operator_task_queue_length',
                                 'The amount of tasks in the task queue.',
                                 ["type"])
metric_task_queue_unready_tasks = Gauge('operator_task_queue_unready_tasks',
                                        'The amount of tasks in the task '
                                        'queue where the run_at timestamp is '
                                        'in the future.')


class ExponentialBackOff:
    """
    Iterable to represent the sleep intervals used for an exponential back-off.

    :param base: The factor by which the back-off value is multiplied on each
        iteration.
    :param start: The initial value.
    :param max_: The maximum value for the back-off.

    This object is iterable. Iterating it will yield values starting at
    `start`, increasing by a factor of `base` until they reach `max_`. At that
    point, only `max_` is returned indefinitely.

    The iterator can be reset to its original value by calling :meth:`reset`.

    .. automethod:: reset

    .. autoattribute:: failing
    """

    __slots__ = ("start", "max_", "base", "_is_failing", "_current")

    def __init__(self, base: int, start: float, max_: int):
        super().__init__()
        self.start = start
        self.max_ = max_
        self.base = base
        self._is_failing = False
        self._current = self.start

    def __iter__(self) -> ExponentialBackOff:
        return self

    def __next__(self) -> float:
        self._is_failing = True
        val = self._current
        self._current = min(self._current * self.base, self.max_)
        return val

    def next(self) -> float:
        return next(self)

    def reset(self) -> None:
        """
        Reset the back-off to its initial state and clear :attr:`failing`.
        """
        self._current = self.start
        self._is_failing = False

    @property
    def failing(self) -> bool:
        """
        Returns true if the iterator has been iterated since the last call to
        :meth:`reset` or if the iterator was just created.
        """
        return self._is_failing


TaskCallable = typing.Callable[..., typing.Awaitable[typing.Optional[bool]]]


class TaskItem(typing.NamedTuple):
    func: TaskCallable
    data: typing.Tuple


@dataclasses.dataclass
class TaskSchedule:
    run_at: float
    backoff: ExponentialBackOff
    force_reschedule: bool = False
    prio: int = 0


@environ.config(prefix="YAOOK_OP_QUEUE")
class TaskQueueConfig:
    backoff_base = environ.var(converter=int, default=2)
    backoff_start = environ.var(converter=float, default=0.5)
    backoff_max = environ.var(converter=int, default=120)
    backoff_jitter = environ.var(converter=float, default=0.1)


def jitter_backoff(dt: float, jitter: float) -> float:
    flexible = dt * jitter
    return random.random() * flexible + dt - flexible / 2  # nosemgrep
    # as the random values are not for cryptgraphic usage


def export_queue_metrics(queue: TaskQueue) -> None:
    now = clock()
    queue_length = len(queue._schedule)
    queue_periodic_tasks = len([schedule
                                for schedule in queue._schedule
                                if schedule[0].prio == 2])
    queue_initial_tasks = len([schedule
                               for schedule in queue._schedule
                               if schedule[0].prio == 1])
    queue_watcher_tasks = len([schedule
                               for schedule in queue._schedule
                               if schedule[0].prio == 0])
    queue_unready_tasks = len([schedule
                              for schedule in queue._schedule
                              if schedule[0].run_at - now > 0])
    metric_task_queue_length.labels("all").set(queue_length)
    metric_task_queue_length.labels("periodic_tasks") \
        .set(queue_periodic_tasks)
    metric_task_queue_length.labels("initial_or_broadcast_tasks") \
        .set(queue_initial_tasks)
    metric_task_queue_length.labels("watcher_tasks") \
        .set(queue_watcher_tasks)
    metric_task_queue_unready_tasks.set(queue_unready_tasks)


def map_eventtype_prio(event_type: str, broadcast: bool) -> int:
    if event_type == "INTERVAL":
        return 2
    if event_type == "INITIAL" or broadcast:
        return 1
    return 0


class TaskQueue:
    """
    De-duplicating and re-trying queue of tasks.

    :param backoff_base: The base of the exponential back-off for failed tasks.
    :param backoff_start: The start value of the exponential back-off for
        failed tasks.
    :param backoff_max: The cutoff value for the exponential back-off for
        failed tasks.
    :param jitter: The jitter ratio to use for rescheduling failed tasks.
    :param logger: The logger to use for logging information about the queue
        itself.

    .. note::

        The queue does not do anything unless :meth:`run` is running.

    Task are initially run in the order they are enqueued.

    If a task is run and fails, it will be rescheduled for a later attempt,
    without blocking other enqueued tasks. Hence, the order of the tasks can
    not be used to enforce dependencies between tasks. Repeatedly failing tasks
    will be rescheduled with exponential back-off, but never removed from the
    queue completely.

    Tasks are rescheduled not in a strict exponential back-off, but are
    slightly jittered based on the `jitter` argument. This is to spread out the
    load on consumed resources even more if several tasks fail at the same
    time.

    .. automethod:: run
    """

    def __init__(self, *,
                 logger: typing.Optional[logging.Logger] = None):
        super().__init__()
        config = environ.to_config(TaskQueueConfig)
        self._tasks: typing.Dict[TaskItem, TaskSchedule] = {}
        self._schedule: typing.List[typing.Tuple[TaskSchedule, TaskItem]] = []
        self._backoff_base = config.backoff_base
        self._backoff_start = config.backoff_start
        self._backoff_max = config.backoff_max
        self._backoff_jitter = config.backoff_jitter
        self._queue_changed = asyncio.Event()
        self.logger = logger or logging.getLogger(__name__)

    def _add(self, task: TaskItem, schedule: TaskSchedule) -> None:
        self._schedule.append((schedule, task))
        self._schedule.sort(key=lambda x: (x[0].prio, x[0].run_at))
        self._tasks[task] = schedule
        # asyncio.Event.set notifies *all* tasks, so we can rely on this waking
        # up all currently sleeping runners.
        # It is important to notify of the queue change here and not in `push`
        # to correctly wake up other runners when a task is re-enqueued after
        # an exception or when it returned True.
        self._queue_changed.set()
        export_queue_metrics(self)

    def push(self, func: TaskCallable, data: typing.Tuple,
             reconcile_reason: str = "", broadcast: bool = False) -> None:
        """
        Push a task into the queue.

        :param func: The coroutine function of the task.
        :param data: Arguments passed to the coroutine function when the task
            is run. Note that this must be hashable.

        A task which is pushed into the queue will run completely at least once
        after the call to :meth:`push` has completed. Task coroutine functions
        are called with the `data` unpacked as their positionional arguments.

        Three cases are distinguished:

        - If the task is not enqueued, it will be added to the queue.
        - If the task is enqueued and not currently running (e.g. because it is
          in an exponential back-off phase), this method does nothing.
        - If the task is enqueued and currently running, this method marks it
          to re-run immediately when it finishes, even if it succeeds.
        """
        task = TaskItem(func, data)
        now = clock()
        prio = map_eventtype_prio(reconcile_reason, broadcast)
        try:  # nosemgrep this is intended as success is failure in this case
            schedule = self._tasks[task]
        except KeyError:
            pass
        else:
            self.logger.debug(
                "%s already in queue (scheduled for %s, which is in %.2fs); "
                " ensuring it will start at least once after this",
                task, schedule.run_at, schedule.run_at - now,
            )
            schedule.force_reschedule = True
            if schedule.prio > prio:
                schedule.prio = prio
            return

        schedule = TaskSchedule(
            now,
            ExponentialBackOff(self._backoff_base,
                               self._backoff_start,
                               self._backoff_max),
            prio=prio
        )
        self._add(task, schedule)

    async def run_next_task(self) -> None:
        """
        Wait for a task to appear in the queue, run it and return.

        This will always return without an exception after one attempt of
        running the task, even if the task fails; the task is then re-enqueued
        into the queue.
        """
        while True:
            # This is, in fact, safe. As we have cooperative concurrency, it is
            # not possible for a wakeup to get lost. For each individual runner
            # holds:
            #
            # - If the queue is empty, it enters a waiting state before
            #   yielding control.
            # - If the queue is not empty and the next task is scheduled for
            #   the future, the runner enters a waiting state before yielding
            #   control.
            # - If the queue is not empty and the next task is scheduled to
            #   run now, the runner executes the task and returns from this
            #   function (if called from `run()`, they will return to the head
            #   of this loop).
            #
            # When a wakeup is triggered, all *currently waiting* tasks will
            # be woken up, no matter the previous state of the Event:
            #
            # - If the event was already `set()`, the runners are waking up
            #   immediately from their `wait()`.
            # - If the event was *not* already `set()` when the wakeup is
            #   triggered, all currently waiting runners will be woken up (as
            #   per asyncio.Event API specification). The runners which are
            #   *not* currently waiting will return from this function (if
            #   called from `run()`, they will return to the head of the loop
            #   and check the queue state independent from any wakeup event
            #   status).
            #
            # Hence, even though this is not using a asyncio.Condition, it is
            # a safe workflow where no wakeups can get lost.
            self._queue_changed.clear()
            now = clock()
            if not self._schedule:
                self.logger.debug(
                    "nothing to do, waiting for something to happen",
                )
                await self._queue_changed.wait()
                continue

            ready_schedule = [schedule
                              for schedule in self._schedule
                              if schedule[0].run_at <= now
                              ]

            if not ready_schedule:
                tmp = self._schedule.copy()
                tmp.sort(key=lambda x: x[0].run_at)
                next_schedule, _ = tmp[0]
                self.logger.debug("next item is scheduled for %s "
                                  "(in %.2fs)", next_schedule.run_at,
                                  next_schedule.run_at - now)

                try:  # this exception is planned here just to wait
                    await asyncio.wait_for(self._queue_changed.wait(),
                                           next_schedule.run_at - now)
                except asyncio.TimeoutError:
                    pass
                continue

            next_schedule, next_task = ready_schedule[0]
            # We remove the item from the schedule while we execute it. This
            # allows for parallel execution of tasks by multiple workers,
            # without sacrificing the exponential back off on failures or
            # risking duplicate execution of tasks.
            schedule, task = next_schedule, next_task
            self._schedule.remove((next_schedule, next_task))
            export_queue_metrics(self)
            # Everything beyond the above line needs to be guarded by the
            # exception block in order to provide the consistency guarantees of
            # the tasks/schedule data structures.
            try:
                if task not in self._tasks:
                    self.logger.error("task %s was in schedule, but not in "
                                      "tasks. dropping.",
                                      task)
                    return

                func = task.func
                data = task.data
                # Clear the flag to prevent an infinite loop. If we find the
                # flag to be true after the task has completed, we know that
                # someone wants us to run it again.
                schedule.force_reschedule = False
                try:
                    requeue = await func(*data)
                except KeyboardInterrupt:
                    # stop immediately
                    raise
                except asyncio.CancelledError:
                    # we are being gently asked to stop, hence, we’ll requeue
                    # this task for immediate re-scheduling (if it ever
                    # happens) and re-raise this
                    self._add(task, schedule)
                    raise
                except Exception:
                    requeue = True
                    backoff = jitter_backoff(
                        next(schedule.backoff),
                        self._backoff_jitter,
                    )
                    self.logger.error("task %s failed. retrying in %rs",
                                      task, backoff, exc_info=True)
                else:
                    # reset the exponential back-off here before resuming the
                    # task as it succeeded once now.
                    schedule.backoff.reset()
                    if requeue:
                        backoff = jitter_backoff(
                            next(schedule.backoff),
                            self._backoff_jitter,
                        )
                        self.logger.debug("task %s requested a requeue. "
                                          "re-running in %rs",
                                          task, backoff)

                if schedule.force_reschedule and not requeue:
                    requeue = True
                    backoff = jitter_backoff(
                        next(schedule.backoff),
                        self._backoff_jitter,
                    )
                    self.logger.debug(
                        "task %s was requested to be scheduled while it was "
                        "running, re-running in %rs",
                        task, backoff,
                    )

                if requeue:
                    schedule.run_at = clock() + backoff
                    schedule.force_reschedule = False
                    self._add(task, schedule)
                else:
                    self._tasks.pop(task, None)
                    self.logger.debug(
                        "task %s completed without error, dropping.",
                        task
                    )
            except asyncio.CancelledError:
                # handled above, re-raise without extra handling
                raise
            except BaseException:
                # if anything fails here, we’ll hard-drop the task from the
                # internal state, since this is probably dangerous.
                self._tasks.pop(task, None)
                self.logger.exception(
                    "task dropped due to unhandled error during task "
                    "processing"
                )
                raise

            return

    async def run(self) -> None:
        """
        Run the queue, forever.
        """
        while True:
            await self.run_next_task()


class RestartingTask:
    """
    Background task which gets restarted with exponential back-off if it
    crashes.

    :param coroutine_function: The coroutine function implementing the task.
    :param logger: A logger used to log internals of the object.
    :param loop: An optional asyncio event loop to use.

    The initial state of the :class:`RestartingTask` is stopped. To start it,
    call the :meth:`start` method. At that point, the `coroutine_function` is
    called without arguments and scheduled to be run by the event loop.

    If the coroutine raises an exception, it will be restarted with an
    exponential back-off (up to 120 seconds). If the coroutine exits without
    error or with a cancellation, it will not be restarted automatically.

    When :meth:`stop` is called and the task is currently running, it will be
    cancelled (and not automatically restarted).

    .. automethod:: start

    .. automethod:: restart

    .. automethod:: stop

    .. automethod:: wait_for_termination

    """

    def __init__(self, coroutine_function: typing.Callable,
                 logger: typing.Optional[logging.Logger] = None,
                 loop: typing.Optional[
                     asyncio.events.AbstractEventLoop] = None):
        super().__init__()
        self._func = coroutine_function
        self._task: typing.Optional[asyncio.Task] = None
        self._should_run = False
        self.backoff = ExponentialBackOff(base=2, start=1, max_=120)
        self._loop = loop or asyncio.get_event_loop()
        self._logger = logger or logging.getLogger(
            ".".join([__name__, type(self).__qualname__, str(id(self))]),
        )

    def start(self) -> None:
        """
        Start the task to run it in the background.

        This resets the internal exponential back-off for crashes and starts
        the task immediately, if it is not runnnig. If the task is already
        running, this method only resets the back-off.
        """
        self._should_run = True
        self.backoff.reset()
        self._ensure_state()

    def stop(self) -> None:
        """
        Stop the task.

        If the task is not running, this does nothing. Otherwise, the task is
        cancelled.
        """
        self._should_run = False
        self._ensure_state()

    def restart(self) -> None:
        """
        Stop and start the task.

        If the task has been stopped before, this is equivalent to calling
        :meth:`start`.
        """
        if not self._should_run:
            return self.start()
        self.backoff.reset()
        if self._task is None:
            self._ensure_state()
        else:
            # cancel the task in order to force an immediate restart
            self._task.cancel()

    async def wait_for_termination(self) -> None:
        """
        Stop the task and wait for it to terminate.

        Exceptions from the task are not propagated but are logged as usual.
        """
        self.stop()
        if self._task is None:
            return
        try:
            await self._task
        except asyncio.CancelledError:
            # CancelledError is not an Exception (it’s a BaseException)
            pass
        except Exception:  # nosemgrep Exceptions are explicity not propagated
            pass

    def _task_done(self, task: asyncio.Task) -> None:
        if task is not self._task:
            raise AssertionError("The finished task is not the task being run")
        try:
            try:
                result = task.result()
            except asyncio.CancelledError:
                self._logger.debug("task stopped after cancellation request")
                if self._should_run:
                    self._logger.info("restarting task immediately because"
                                      " the desired state is up")
                    self._loop.call_soon(self._ensure_state)
                return
            except BaseException:
                delay = next(self.backoff)
                self._logger.error("task crashed! retrying in %ss",
                                   delay, exc_info=True)
                self._loop.call_later(delay, self._ensure_state)
                return

            self.backoff.reset()
            if result:
                delay = next(self.backoff)
                self._logger.info("task exited with result %r, "
                                  "restarting in %ss",
                                  result, delay)
                self._loop.call_later(delay, self._ensure_state)
            else:
                self._logger.info("task exited with result %r, not restarting",
                                  result)
                self._should_run = False
        finally:
            self._task = None

    def _ensure_state(self) -> None:
        if self._task is None and self._should_run:
            # need to start task
            self._task = self._loop.create_task(self._func())
            self._task.add_done_callback(self._task_done)
        elif self._task is not None and not self._should_run:
            # need to stop task
            self._task.cancel()
