"""
:mod:`~yaook.op.scheduling_keys` -- Scheduling keys shared among operators
##################################################################

.. autoclass:: SchedulingKey()
"""
import enum


class SchedulingKey(enum.Enum):
    """
    This enumeration holds all scheduling keys used by the Yaook operators.

    .. seealso::

        :ref:`concepts.scheduling`
            for a general discussion of how Yaook handles scheduling.


    **Operators**

    .. autoattribute:: OPERATOR_ANY

    .. autoattribute:: OPERATOR_BARBICAN

    .. autoattribute:: OPERATOR_CINDER

    .. autoattribute:: OPERATOR_CONFIGURED_DAEMON_SET

    .. autoattribute:: OPERATOR_GLANCE

    .. autoattribute:: OPERATOR_HEAT

    .. autoattribute:: OPERATOR_INFRA_IRONIC

    .. autoattribute:: OPERATOR_KEYSTONE

    .. autoattribute:: OPERATOR_NEUTRON

    .. autoattribute:: OPERATOR_NOVA

    .. autoattribute:: OPERATOR_GNOCCHI

    .. autoattribute:: OPERATOR_CEILOMETER

    **Infrastructure services**

    .. autoattribute:: ANY_INFRA

    .. autoattribute:: INFRA_DATABASE

    .. autoattribute:: INFRA_OVSDB_CLUSTER

    .. autoattribute:: INFRA_MESSAGE_QUEUE

    **API Services**

    .. autoattribute:: ANY_API

    .. autoattribute:: BLOCK_STORAGE_API

    .. autoattribute:: IMAGE_API

    .. autoattribute:: INFRA_BARE_METAL_API

    .. autoattribute:: IDENTITY_API

    .. autoattribute:: NETWORK_API

    .. autoattribute:: COMPUTE_API

    .. autoattribute:: COMPUTE_METADATA_API

    .. autoattribute:: KEY_MANAGER_API

    .. autoattribute:: PLACEMENT_API

    .. autoattribute:: GNOCCHI_API

    **Barbican supporting services**

    .. autoattribute:: KEY_MANAGER_BARBICAN_ANY_SERVICE

    .. autoattribute:: KEY_MANAGER_BARBICAN_KEYSTONE_LISTENER

    **Cinder supporting services**

    .. autoattribute:: BLOCK_STORAGE_CINDER_ANY_SERVICE

    .. autoattribute:: BLOCK_STORAGE_CINDER_BACKUP

    .. autoattribute:: BLOCK_STORAGE_CINDER_SCHEDULER

    .. autoattribute:: BLOCK_STORAGE_CINDER_VOLUME

    **Infrastructure Ironic supporting services**

    .. autoattribute:: INFRA_BARE_METAL_IRONIC_ANY_SERVICE

    .. autoattribute:: INFRA_BARE_METAL_IRONIC_CONDUCTOR

    **Nova supporting services**

    .. autoattribute:: COMPUTE_NOVA_ANY_SERVICE

    .. autoattribute:: COMPUTE_NOVA_CONDUCTOR

    .. autoattribute:: COMPUTE_NOVA_SCHEDULER

    .. autoattribute:: COMPUTE_NOVA_CONSOLEAUTH

    .. autoattribute:: COMPUTE_VNC

    **Network services**

    .. autoattribute:: NETWORK_NEUTRON_L2_AGENT

    .. autoattribute:: NETWORK_NEUTRON_L3_AGENT

    .. autoattribute:: NETWORK_NEUTRON_DHCP_AGENT

    .. autoattribute:: NETWORK_NEUTRON_BGP_DRAGENT

    .. autoattribute:: NETWORK_NEUTRON_BGP_AGENT

    .. autoattribute:: NETWORK_NEUTRON_NORTHD

    .. autoattribute:: NETWORK_NEUTRON_OVN_AGENT

    **Compute services**

    .. autoattribute:: COMPUTE_HYPERVISOR

    **Gnocchi supporting services**

    .. autoattribute:: GNOCCHI_METRICD

    **Ceilometer services**

    .. autoattribute:: CEILOMETER_ANY_SERVICE

    .. autoattribute:: CEILOMETER_NOTIFICATION

    .. autoattribute:: CEILOMETER_CENTRAL

    .. note::

        If we ever support other hypervisor technologies than Libvirt, the
        Libvirt service will get its own label and Nova Compute may use a
        scheduling similar to the Neutron L2 Agent to appear on all relevant
        nodes.

    """
    # The key structure is: ${SERVICE}_${COMPONENT}
    #
    # SERVICE is the name of the provided services **not** of the
    # implementation.
    #
    # If the COMPONENT is not a well-known component (see below), it must be
    # prefixed by the implementation name (e.g. `nova`).
    #
    # Well known components are:
    #
    # - api
    # - metadata-api (compute only)
    #
    # The values follow the same concept: ${SERVICE}.yaook.cloud/${COMPONENT},
    # though they use dashes instead of underscores.

    BLOCK_STORAGE_CINDER_BACKUP = "block-storage.yaook.cloud/cinder-backup"
    """
    Used to schedule the ``cinder-backup`` service.
    """

    BLOCK_STORAGE_CINDER_SCHEDULER = \
        "block-storage.yaook.cloud/cinder-scheduler"
    """
    Used to schedule the ``cinder-scheduler`` service.
    """

    BLOCK_STORAGE_CINDER_VOLUME = "block-storage.yaook.cloud/cinder-volume"
    """
    Used to schedule the ``cinder-volume`` service.
    """

    BLOCK_STORAGE_API = "block-storage.yaook.cloud/api"
    """
    Used to schedule the ``cinder-api`` Deployment.
    """

    COMPUTE_API = "compute.yaook.cloud/api"
    """
    Used to schedule the ``nova-api`` Deployment.
    """

    COMPUTE_HYPERVISOR = "compute.yaook.cloud/hypervisor"
    """
    Used to schedule the ``nova-compute`` and ``libvirt`` Statefulset.
    """

    COMPUTE_METADATA_API = "compute.yaook.cloud/metadata-api"
    """
    Used to schedule the ``nova-metadata-api`` Deployment.
    """

    COMPUTE_NOVA_CONDUCTOR = "compute.yaook.cloud/nova-conductor"
    """
    Used to schedule the ``nova-conductor`` Deployment.
    """

    COMPUTE_NOVA_SCHEDULER = "compute.yaook.cloud/nova-scheduler"
    """
    Used to schedule the ``nova-scheduler`` Deployment.
    """

    COMPUTE_NOVA_CONSOLEAUTH = "compute.yaook.cloud/nova-consoleauth"
    """
    Used to schedule the ``nova-consoleauth`` Deployment.
    """

    COMPUTE_VNC = "compute.yaook.cloud/vnc"
    """
    Used to schedule the ``vnc`` Deployment.
    """

    HEAT_API = "heat.yaook.cloud/api"
    """
    Used to schedule the ``heat-api`` Deployment.
    """

    HEAT_API_CFN = "heat.yaook.cloud/api-cfn"
    """
    Used to schedule the ``heat-api-cfn`` Deployment.
    """

    HEAT_ENGINE = "heat.yaook.cloud/engine"
    """
    Used to schedule the ``heat-engine`` Deployment.
    """

    IDENTITY_API = "identity.yaook.cloud/api"
    """
    Used to schedule the ``keystone-api`` Deployment.
    """

    INFRA_DATABASE = "infra.yaook.cloud/db"
    """
    Used to schedule database-related services.
    """

    INFRA_OVSDB_CLUSTER = "infra.yaook.cloud/ovsdbcluster"
    """
    Used to schedule ovsdb-server raft clusters.
    """

    INFRA_MESSAGE_QUEUE = "infra.yaook.cloud/mq"
    """
    Used to schedule services related to or implementing message queues.
    """

    INFRA_CACHING = "infra.yaook.cloud/caching"
    """
    Used to schedule services related to or implementing caching.
    """

    INFRA_BARE_METAL_API = "infra-bare-metal.yaook.cloud/api"
    """
    Used to schedule the ``infra-ironic-api`` Deployment.
    """

    INFRA_BARE_METAL_IRONIC_CONDUCTOR = "infra-bare-metal.yaook.cloud/conductor"  # noqa:E501
    """
    Used to schedule the ``infra-ironic-conductor`` Statefulset.
    """

    IMAGE_API = "image.yaook.cloud/api"
    """
    Used to schedule the ``glance-api`` Deployment.
    """

    KEY_MANAGER_API = "key-manager.yaook.cloud/api"
    """
    Used to schedule the ``barbican-api`` deployment.
    """

    KEY_MANAGER_BARBICAN_KEYSTONE_LISTENER = \
        "key-manager.yaook.cloud/barbican-keystone-listener"
    """
    Used to schedule the ``barbican-keystone-listener`` service.
    """

    NETWORK_API = "network.yaook.cloud/api"
    """
    Used to schedule the ``neutron-api`` Deployment.
    """

    NETWORK_NEUTRON_DHCP_AGENT = "network.yaook.cloud/neutron-dhcp-agent"
    """
    Used to schedule the ``neutron-dhcp-agent`` Statefulset.
    """

    NETWORK_NEUTRON_L2_AGENT = "network.yaook.cloud/neutron-l2-agent"
    """
    Used to schedule the ``neutron-l2-agent`` Statefulset, among other
    scheduling keys.

    .. seealso::

       :ref:`concepts.scheduling.neutron-l2-agent`
        for more details on L2 agent scheduling
    """

    NETWORK_NEUTRON_L3_AGENT = "network.yaook.cloud/neutron-l3-agent"
    """
    Used to schedule the ``neutron-l3-agent`` Statefulset.
    """

    NETWORK_NEUTRON_BGP_DRAGENT = "network.yaook.cloud/neutron-bgp-dragent"
    """
    Used to schedule the ``neutron-bgp-dragent`` Statefulset.
    """

    NETWORK_NEUTRON_BGP_AGENT = "network.yaook.cloud/neutron-bgp-agent"
    """
    Used to schedule the ``neutron-bgp-dragent`` or ``neutron-ovn-bgp``
    Statefulset.
    """

    NETWORK_NEUTRON_NORTHD = "network.yaook.cloud/neutron-northd"
    """
    Used to schedule the ``ovn-northd`` Deployment.
    """

    NETWORK_NEUTRON_OVN_AGENT = "network.yaook.cloud/neutron-ovn-agent"
    """
    Used to schedule the ``ovn-controller``, ``openvswitchd`` and ``ovs-db``
    Statefulsets.
    """

    NETWORK_NEUTRON_NETWORK_NODE = "network.yaook.cloud/neutron-network-node"
    """
    Used to schedule ``neutron-dhcp-agent`, ``neutron-l3-agent`` and
    (if configured) ``neutron-bgp-dragent`` or ``neutron-ovn-agent`` if ovn
    is configured to be used.
    """

    OPERATOR_BARBICAN = "operator.yaook.cloud/barbican"
    """
    Used to schedule ``yaook.op.barbican`` Jobs.
    """

    OPERATOR_CINDER = "operator.yaook.cloud/cinder"
    """
    Used to schedule ``yaook.op.cinder`` Jobs.
    """

    OPERATOR_CONFIGURED_DAEMON_SET = \
        "operator.yaook.cloud/configured-daemon-set"

    OPERATOR_GLANCE = "operator.yaook.cloud/glance"
    """
    Used to schedule ``yaook.op.glance`` Jobs.
    """

    GNOCCHI_API = "gnocchi.yaook.cloud/api"
    """
    Used to schedule ``gnocchi-api`` Deployment.
    """

    OPERATOR_GNOCCHI = "operator.yaook.cloud/gnocchi"
    """
    Used to schedule ``yaook.op.gnocchi`` Jobs.
    """

    OPERATOR_CEILOMETER = "operator.yaook.cloud/ceilometer"
    """
    Used to schedule ``yaook.op.ceilometer`` Jobs.
    """

    OPERATOR_HEAT = "operator.yaook.cloud/heat"
    """
    Used to schedule ``yaook.op.heat`` Jobs.
    """

    OPERATOR_HORIZON = "operator.yaook.cloud/horizon"
    """
    Used to schedule ``yaook.op.horizon`` Jobs.
    """

    OPERATOR_INFRA = "operator.yaook.cloud/infra"
    """
    Used to schedule ``yaook.op.infra`` Jobs.
    """

    OPERATOR_INFRA_IRONIC = "operator.yaook.cloud/infra-ironic"
    """
    Used to schedule ``yaook.op.infra_ironic`` Jobs.
    """

    OPERATOR_KEYSTONE = "operator.yaook.cloud/keystone"
    """
    Used to schedule ``yaook.op.keystone`` Jobs.
    """

    OPERATOR_NEUTRON = "operator.yaook.cloud/neutron"
    """
    Used to schedule ``yaook.op.neutron`` Jobs.
    """

    OPERATOR_NOVA = "operator.yaook.cloud/nova"
    """
    Used to schedule ``yaook.op.nova`` Jobs.
    """

    OPERATOR_TEMPEST = "operator.yaook.cloud/tempest"
    """
    Used to schedule ``yaook.op.tempest`` Jobs.
    """

    PLACEMENT_API = "placement.yaook.cloud/api"
    """
    Used to schedule the ``placement-api`` Deployment.
    """

    DASHBOARD = "dashboard.yaook.cloud/ui"
    """
    Used to schedule the ``horizon`` Deployment.
    """

    CEILOMETER_CENTRAL = "ceilometer.yaook.cloud/central"
    """
    Used to schedule the ``ceilometer-central`` Deployment.
    """

    CEILOMETER_NOTIFICATION = "ceilometer.yaook.cloud/notification"
    """
    Used to schedule the ``ceilometer-notification`` Deployment.
    """

    GNOCCHI_METRICD = "gnocchi.yaook.cloud/metricd"
    """
    Used to schedule the ``gnocchi-metricd`` Deployment.
    """

    # Wildcards
    ANY_API = "any.yaook.cloud/api"
    """
    Used to schedule any of the OpenStack API service deployments.
    """

    ANY_INFRA = "infra.yaook.cloud/any"
    """
    Used to schedule any infrastructure component.
    """

    OPERATOR_ANY = "operator.yaook.cloud/any"
    """
    Used to schedule any of the Yaook operator Jobs.
    """

    COMPUTE_NOVA_ANY_SERVICE = "compute.yaook.cloud/nova-any-service"
    """
    Used to schedule any of the Nova supporting services (conductor,
    scheduler).
    """

    INFRA_BARE_METAL_IRONIC_ANY_SERVICE = "infra-bare-metal.yaook.cloud/ironic-any-service"  # noqa:E501
    """
    Used to schedule any of the Ironic supporting services for the
    infrastructure bare metal service.
    """

    BLOCK_STORAGE_CINDER_ANY_SERVICE = \
        "block-storage.yaook.cloud/cinder-any-service"
    """
    Used to schedule any of the Cinder supporting services (backup, volume,
    scheduler).
    """

    CEILOMETER_ANY_SERVICE = "ceilometer.yaook.cloud/ceilometer-any-service"
    """
    Used to schedule any of the Ceilometer services (central, notification).
    """

    KEY_MANAGER_BARBICAN_ANY_SERVICE = \
        "key-manager.yaook.cloud/barbican-any-service"
    """
    Used to schedule any of the barbican supporting services
    (barbican-keystone-listener).
    """

    NEUTRON_ANY_SERVICE = "network.yaook.cloud/any"
    """
    Used to schedule any network component.
    """


L3_SCHEDULING_KEYS = [
    SchedulingKey.NETWORK_NEUTRON_L3_AGENT.value,
    SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value,
]

DHCP_SCHEDULING_KEYS = [
    SchedulingKey.NETWORK_NEUTRON_DHCP_AGENT.value,
    SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value,
]

BGP_SCHEDULING_KEYS = [
    SchedulingKey.NETWORK_NEUTRON_BGP_DRAGENT.value,
    SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value,
]

OVN_BGP_SCHEDULING_KEYS = [
    SchedulingKey.NETWORK_NEUTRON_BGP_AGENT.value,
    SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value,
]

L2_SCHEDULING_KEYS = sorted(set(
    L3_SCHEDULING_KEYS + DHCP_SCHEDULING_KEYS + BGP_SCHEDULING_KEYS +
    [
        SchedulingKey.NETWORK_NEUTRON_L2_AGENT.value,
        SchedulingKey.COMPUTE_HYPERVISOR.value,
    ]))

OVN_SCHEDULING_KEYS = [
    SchedulingKey.NETWORK_NEUTRON_OVN_AGENT.value,
    SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value,
    SchedulingKey.COMPUTE_HYPERVISOR.value,
]

ANY_SCHEDULING_KEYS = [
    SchedulingKey.OPERATOR_ANY.value,
    SchedulingKey.ANY_INFRA.value,
    SchedulingKey.ANY_API.value,
    SchedulingKey.KEY_MANAGER_BARBICAN_ANY_SERVICE.value,
    SchedulingKey.BLOCK_STORAGE_CINDER_ANY_SERVICE.value,
    SchedulingKey.INFRA_BARE_METAL_IRONIC_ANY_SERVICE.value,
    SchedulingKey.COMPUTE_NOVA_ANY_SERVICE.value,
    SchedulingKey.CEILOMETER_ANY_SERVICE.value,
    SchedulingKey.NEUTRON_ANY_SERVICE.value,
]
