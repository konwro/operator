#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import typing

from oslo_config import cfg
from oslo_policy import policy, _checks

from yaook.statemachine import exceptions


def build_policy_configmap(
        policies: typing.Mapping[str, str],
        defaults_filename: str,
        policy_filename: str = "policy.json",
        ) -> typing.Mapping[str, str]:

    try:
        valid_policies = validate_policies(policies, defaults_filename)
    # Who needs common base classes :)
    except (policy.PolicyNotAuthorized, policy.InvalidScope,
            policy.DuplicatePolicyError, policy.PolicyNotRegistered,
            policy.InvalidDefinitionError, policy.InvalidRuleDefault,
            policy.InvalidContextObject) as e:
        raise exceptions.ConfigurationInvalid(str(e))

    if not policy_filename:
        raise ValueError("Policy filename must not be empty.")

    # Use the result from `validate_policies` which also includes the defaults.
    return {f"{policy_filename}": json.dumps(valid_policies)}


def validate_policies(
        unvalidated: typing.Mapping[str, str],
        default_policy_filename: str) -> typing.Mapping[str, str]:
    """Validate the unvalidated policies together with the default policies.

    :param unvalidated: policies to validate
    :param default_policy_filename: name of file containing default policies

    :return: The validated policies including the defaults
    """
    defaults = get_default_policies(default_policy_filename)
    enforcer = policy.Enforcer(
        cfg.CONF,
        policy_file=default_policy_filename,
        use_conf=False,
    )

    # all_unvalidated_dict is to contain all rules in defaults (with possibly
    # modified values) together with the rules only defined in unvalidated.
    all_unvalidated_dict = dict(defaults)
    all_unvalidated_dict.update(unvalidated)

    # Check that no invalid rules are defined.
    all_unvalidated_rules = policy.Rules.from_dict(all_unvalidated_dict)
    _validate_policy_keys(all_unvalidated_rules, defaults)

    # Check that the rules are non-cyclic and that all referenced
    # rules are defined.
    enforcer.set_rules(all_unvalidated_rules)
    enforcer.check_rules(raise_on_violation=True)

    # The enforcer.check_rules and the parsing of the rules that
    # Rules.from_dict() performs are not enough to validate the provided
    # policy.
    # For example, the rule value "dummy" cannot be parsed in Rules.from_dict()
    # which results in the rule having the value _checks.FalseCheck(). This is
    # however not considered invalid. We want to be more conservative and
    # disallow such values. Therefore we validate the policy values again.
    _validate_policy_values(all_unvalidated_dict, all_unvalidated_rules)

    return all_unvalidated_dict


def _get_rule_checks(check: policy.Check) -> typing.List[policy.RuleCheck]:
    if isinstance(check, policy.RuleCheck):
        return [check]
    rule_checks = []
    rule = getattr(check, 'rule', None)
    if rule:
        rule_checks += _get_rule_checks(rule)
    rules = getattr(check, 'rules', None)
    if rules:
        for c in rules:
            rule_checks += _get_rule_checks(c)
    return rule_checks


def _validate_policy_keys(
        all_unvalidated_rules: policy.Rules,
        defaults: typing.Mapping[str, str]) -> None:
    """Makes sure that no rule key name in all_unvalidated_rules, but not in
    defaults, is unused, i.e., never mentioned in any of the rule values."""
    non_defaults = {
        rule_name
        for rule_name in all_unvalidated_rules
        if rule_name not in defaults
    }

    all_rule_checks = {
        rule_name: _get_rule_checks(check)
        for rule_name, check in all_unvalidated_rules.items()
    }
    used_policy_rules = {
        check.match
        for using_rule_name, rule_checks in all_rule_checks.items()
        for check in rule_checks
    }
    unused_policy_rules = []
    for rule_name in non_defaults:
        if rule_name not in used_policy_rules:
            unused_policy_rules.append(rule_name)

    if unused_policy_rules:
        raise policy.PolicyNotRegistered(unused_policy_rules)


def _validate_policy_values(
        unvalidated_dict: typing.Mapping[str, str],
        unvalidated_rules: policy.Rules) -> None:
    for rule in unvalidated_rules:
        check = unvalidated_rules.get(rule, _checks.FalseCheck)
        if isinstance(check, _checks.FalseCheck):
            # The value of the rule 'rule' is either malformed or
            # correctly specified as a rule which 'never allows', i.e., as '!'
            # (the string representation of oslo_policy._checks.FalseCheck()).
            # Therefore we raise unless the rule was explicitly given as '!'.
            if unvalidated_dict[rule] != str(_checks.FalseCheck()):
                raise policy.InvalidDefinitionError([rule])


def get_default_policies(file_path: str) -> typing.Mapping[str, str]:
    """Returns the policies in the provided file.

    Raises ValueError if the file_path is invalid or the file
    does not contain any policies, since the default policy file
    must not be empty.

    :param file_path: path to file containing the default policies
    :return: the default policies
    :raises: FileNotFoundError
    """
    with open(file_path) as f:
        default_policies_str = f.read()
        default_policies = policy.parse_file_contents(default_policies_str)
        if not default_policies:
            msg = f"The default policy file '{file_path}' must not be empty."
            raise ValueError(msg)
        return default_policies
