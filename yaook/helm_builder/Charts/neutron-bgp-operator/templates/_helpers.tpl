{{- define "yaook.operator.name" -}}
neutron_bgp
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_NEUTRON_BGP_DRAGENT_OP_JOB_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
