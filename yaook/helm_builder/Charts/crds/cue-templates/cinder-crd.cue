// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#messagequeue
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "CinderDeployment"
	#plural:   "cinderdeployments"
	#singular: "cinderdeployment"
	#shortnames: ["cinderd", "cinderds"]
	#releases: ["train", "ussuri", "victoria", "wallaby", "xena", "yoga", "zed"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"scheduler",
			"targetRelease",
			"cinderConfig",
			"backends",
			"database",
			"messageQueue",
			"memcached",
			"region",
			"issuerRef",
			"backup",
			"databaseCleanup",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			scheduler:      crd.replicated
			scheduler:      crd.topologySpreadConstraints
			cinderConfig: {
				crd.#anyconfig
				#description: "Cinder OpenStack config. "
			}
			cinderSecrets: crd.#configsecret

			databaseCleanup: {
				type: "object"
				required: ["schedule", "deletionTimeRange"]
				description: "Configuration of a periodic database cleanup job, using cinder-manage."
				properties: {
					schedule: crd.#cronschedulesnip
					deletionTimeRange: {
						type:        "integer"
						description: "Deleted database rows older than this number of days will be removed."
						default:     60
					}
				}
			}

			backends: {
				type:        "object"
				description: "Cinder-volume services ('backends') to deploy. Each key is a backend/cinder-volume service; the specific backend which is used is determined by the value."
				additionalProperties: {
					type:        "object"
					description: "Cinder-volume service ('backend') configuration. Exactly one of ``rbd`` and ``netapp`` must be set in order for the configuration to be valid, as that determines the type of backend which is used."
					required: ["volume"]
					oneOf: [
						{required: ["rbd"]},
						{required: ["netapp"]},
					]
					properties: {
						cinderSecrets: crd.#configsecret
						volume: {
							crd.replicated
							crd.topologySpreadConstraints
							description: "Configure the cinder-volume deployment for this backend."
							properties: replicas: {
								default: 1
								minimum: 0
								maximum: 1
							}
							properties: resources: {
								type:        "object"
								description: "Resource requests/limits for the containers related to cinder-volume."
								properties: "cinder-volume": crd.#containerresources
							}
						}
						rbd: {
							type:        "object"
							description: "Ceph RBD backend"
							required: [
								"keyringReference",
								"keyringUsername"]
							properties: {
								backendConfig: {
									type:        "object"
									description: "Extra configuration values to add to the Cinder configuration section for this backend. For global configuration, use the spec.cinderConfig field instead."
									additionalProperties: {
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
								keyringReference: {
									type:        "string"
									description: "Name of the Kubernetes secret containing the Ceph keyring to use. The secret must be in ``kubernetes.io/rook`` format."
								}
								keyringUsername: {
									type:        "string"
									description: "RADOS username to use for authentication."
								}
								cephConfig: {
									type:        "object"
									description: "Additional configuration to add to the ceph configuration for this client. This cannot be used for global ceph configuration. Most common use is to add a ``mon_host`` key here in order to define where to find the mons."
									additionalProperties: {
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
							}
						}
						netapp: {
							type: "object"
							required: [
								"login",
								"passwordReference",
								"server",
								"vserver",
								"shares",
							]
							properties: {
								backendConfig: {
									type:        "object"
									description: "Extra configuration values to add to the Cinder configuration section for this backend. For global configuration, use the spec.cinderConfig field instead."
									additionalProperties: {
										type:                                   "string"
										"x-kubernetes-preserve-unknown-fields": true
									}
								}
								login: {
									type:        "string"
									description: "Username to use for logging into the netapp"
								}
								passwordReference: {
									type:        "string"
									description: "Reference to a secret containing the password for the netapp login."
								}
								server: {
									type:        "string"
									description: "FQDN of the netapp management api."
								}
								vserver: {
									type:        "string"
									description: "The vserver of the netapp to use for this backend"
								}
								copyoffloadConfigMap: {
									crd.#ref
									description: "Reference to a configmap containing the copyoffload binary from netapp. Since this binary is proprietary we can not distribute it with yaook and you need to manually download it and put it into a configmap."
								}
								shares: {
									type:        "array"
									description: "A list of all shares that this backend exports"
									items: type: "string"
								}

							}
						}
					}
				}
			}

			backup: {
				// We use an object here as we need to support multiple different cinder-backup services.
				// This is required as a single cinder-backup can not handle multiple availability zones.
				// We could also use an array, but that would have the issue that we can not easily track the individual entries if one would be removed.
				type:        "object"
				description: "Cinder-backup services to deploy. Each key is a cinder-backup service."
				additionalProperties: {
					type:        "object"
					description: "Cinder-backup service deployment configuration."
					required: ["cinderConfig"]
					crd.replicated
					crd.topologySpreadConstraints
					properties: {
						cinderConfig: {
							crd.#anyconfig
							#description: "Cinder Backup OpenStack config. "
						}
						cinderSecrets: crd.#configsecret
						terminationGracePeriod: {
							type:    "integer"
							default: 3600
						}
						resources: {
							type:        "object"
							description: "Resource requests/limits for containers related to the Cinder Backup service."
							properties: "cinder-backup": crd.#containerresources
						}
					}
				}
			}

			ids: {
				type: "object"
				anyOf: [
					{required: ["uid", "gid"]},
				]
				properties: {
					uid: type: "integer"
					gid: type: "integer"
				}
			}

			api: {
				description: "Cinder API deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits for containers related to the Cinder API."
					properties: {
						"cinder-api":              crd.#containerresources
						"ssl-terminator":          crd.#containerresources
						"ssl-terminator-external": crd.#containerresources
						"ssl-terminator-internal": crd.#containerresources
						"service-reload":          crd.#containerresources
						"service-reload-external": crd.#containerresources
						"service-reload-internal": crd.#containerresources
					}
				}
			}
			scheduler: {
				description: "Cinder Scheduler deployment configuration"
				properties: resources: {
					type:        "object"
					description: "Resource requests/limits for containers related to the Cinder Scheduler."
					properties: "cinder-scheduler": crd.#containerresources
				}
			}
			jobResources: {
				type:        "object"
				description: "Resource limits for Job Pod containers spawned by the Operator"
				properties: {
					"cinder-db-sync-job":         crd.#containerresources
					"cinder-db-upgrade-pre-job":  crd.#containerresources
					"cinder-db-upgrade-post-job": crd.#containerresources
					"cinder-db-cleanup-cronjob":  crd.#containerresources
				}
			}
			conversionVolume: {
				type: "object"
				required: ["emptyDir"]
				properties: {
					emptyDir: {
						type:        "object"
						description: "EmptyDir represents a temporary directory that shares a pod's lifetime. More info: https://kubernetes.io/docs/concepts/storage/volumes#emptydir"
						properties: {
							medium: {
								type:        "string"
								pattern:     "^$|Memory"
								default:     ""
								description: "What type of storage medium should back this directory."
							}
							sizeLimit: {
								type:        "string"
								pattern:     crd.quantilepattern
								description: "Total amount of local storage required for this EmptyDir volume."
							}
						}
					}
				}
			}
		}
	}
}
